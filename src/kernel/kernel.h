#pragma once


#include "..\api\hal.h"
#include "..\api\api.h"
#include "file_system.h"


#ifdef _DEBUG
#include <iostream>
#define Debug_Log(log) (std::cout << "[debug] " << log << std::endl)
#else
#define Debug_Log(log)
#endif


#include<Windows.h>

extern HMODULE User_Programs;

void __stdcall Sys_Call(kiv_hal::TRegisters& regs);
void Set_Error(kiv_hal::TRegisters &regs, kiv_os::NOS_Error error);
void Reset_Error(kiv_hal::TRegisters& regs);
void __stdcall Bootstrap_Loader(kiv_hal::TRegisters &context);
File_System *Get_File_System();