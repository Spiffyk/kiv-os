#pragma once

#include <vector>
#include <mutex>

#include "Stream.h"

/**
* A stream that operates over a fixed-size in-memory area.
*/
class Inmemory_Stream : public Stream {

	std::recursive_mutex mutex;
	bool terminated = false;

	void* area;
	const size_t area_size;
	size_t position = 0;

	const bool writable;

public:

	/**
	* Creates a new stream operating over a newly allocated in-memory area of the specified size.
	* 
	* @param size - the size of the in-memory area
	* @param writable - whether the stream should be able to write into the area
	*/
	explicit Inmemory_Stream(size_t size, bool writable);

	/**
	* Creates a new stream operating over a newly allocated in-memory area of the specified size, pre-filled with data
	* copied from the specified source buffer.
	* 
	* @param src_buffer - the buffer to copy data from
	* @param size - the size of the in-memory area as well as the source buffer
	* @param writable - whether the stream should be able to write into the area
	*/
	explicit Inmemory_Stream(void* src_buffer, size_t size, bool writable);

	/**
	* Creates a new stream operating over a newly allocated in-memory area filled by the data of the specified vector.
	* The size of the area is made to exactly accomodate the elements of the vector.
	* 
	* @param vector - the source vector
	* @param writable - whether the stream should be able to write into the area
	*/
	template <typename T>
	explicit Inmemory_Stream(const std::vector<T>& vector, bool writable)
		: Inmemory_Stream(sizeof(T) * vector.size(), writable)
	{
		memcpy(this->area, vector.data(), this->area_size);
	}


	/**
	* Gets a pointer to the area this stream operates over. May be used to prepare data for the stream to read.
	*/
	void* Get_Area();

	/**
	* Gets the size of the area this stream operates over.
	*/
	size_t Get_Area_Size() const;


	kiv_os::NOS_Error Write(const void* buffer, const size_t size, size_t& written) override;

	kiv_os::NOS_Error Read(void* buffer, const size_t size, size_t& read) override;

	kiv_os::NOS_Error Seek(const int64_t offset, const kiv_os::NFile_Seek whence) override;

	kiv_os::NOS_Error Resize(const int64_t offset, const kiv_os::NFile_Seek whence) override;

	int64_t Get_Seek() override;

	bool Is_Writable() override;

	bool Is_Readable() override;

	bool Is_Seekable() override;

	bool Is_Resizable() override;

	void Terminate() override;

	bool Is_Terminated() override;

	/**
	* Closes the stream and frees the associated memory area.
	*/
	virtual ~Inmemory_Stream();

};
