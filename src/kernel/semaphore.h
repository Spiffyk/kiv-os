#pragma once

#include <mutex>
#include <condition_variable>

template <typename T>
class Semaphore {

	std::mutex mutex;
	std::condition_variable cv;
	T value;
	bool terminated = false;

public:

	/**
	* Creates a new semaphore with the specified initial value.
	*/
	Semaphore(const T init_value) : value(init_value) {
		// nothing more to do
	}

	/**
	* Increments the value of the semaphore. Notifies a single waiting thread.
	*/
	void operator++() {
		{
			std::lock_guard<decltype(mutex)> lck(mutex);
			++value;
		}
		cv.notify_one();
	}

	/**
	* Decrements the value of the semaphore. If the value is zero, blocks the current thread until it is incremented.
	* 
	* @return `true` if woken normally, `false` if terminated
	*/
	bool operator--() {
		std::unique_lock<decltype(mutex)> lck(mutex);
		cv.wait(lck, [this]() { return this->value > 0 || terminated; });
		if (value <= 0) {
			return false;
		} else {
			--value;
			return true;
		}
	}

	void Terminate() {
		{
			std::unique_lock<decltype(mutex)> lck(mutex);
			terminated = true;
		}
		cv.notify_all();
	}
};
