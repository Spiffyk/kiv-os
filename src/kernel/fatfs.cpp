#include "fatfs.h"
#include <iostream>

#define SELF_NAME ".           "
#define PARENT_NAME "..         "

kiv_os::NOS_Error Fat_FS::Init() {
    std::lock_guard<std::mutex> lock(fat_fs_mutex);

    kiv_fat::Boot_Record bpb;
    std::vector <uint16_t> fat_table;
    return kiv_fat::Load(diskNum, mParams, bpb, fat_table);
}

std::string getFileName(fs::path path) {
    std::string p = path.string();
    std::string e = path.extension().string();

    p.resize(p.length() - e.length());
    if (path.has_extension()) {
        e.erase(0, 1);
    }
    p.resize(FILE_NAME_LENGTH - e.length(), 0x20);
    p.append(e);

    std::transform(p.begin(), p.end(), p.begin(), ::toupper);

    return p;
}

bool isReadOnly(uint8_t attrs) {
    return attrs & static_cast<uint8_t>(kiv_os::NFile_Attributes::Read_Only);
}

bool isDirectory(uint8_t attrs) {
    return attrs & static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory);
}

bool isReadOnly(kiv_os::NFile_Attributes attrs) {
    return isReadOnly(static_cast<uint8_t>(attrs));
}

bool isDirectory(kiv_os::NFile_Attributes attrs) {
    return isDirectory(static_cast<uint8_t>(attrs));
}

bool isReadOnly(kiv_fat::Dir_Entry file) {
    return isReadOnly(file.attrs);
}

bool isDirectory(kiv_fat::Dir_Entry file) {
    return isDirectory(file.attrs);
}

kiv_os::NOS_Error Read_Dir(uint8_t disk_num, const kiv_fat::Boot_Record& bpb, const uint16_t* fat_table, kiv_fat::Dir_Entry dir,
    std::vector <kiv_os::TDir_Entry>& entries) {
    std::vector <kiv_fat::Dir_Entry> dir_items;
    kiv_os::NOS_Error status = kiv_fat::Read_Dir(disk_num, bpb, fat_table, dir, dir_items);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    kiv_os::TDir_Entry e;
    entries.clear();

    for (auto& entry : dir_items) {
        if (entry.name[0] != '\0') {
            e.file_attributes = entry.attrs;
            std::fill(e.file_name, e.file_name + 12, 0x20);
            std::memcpy(e.file_name, entry.name, FILE_NAME_LENGTH);
            entries.push_back(e);
        }
    }

    return kiv_os::NOS_Error::Success;
}

kiv_os::NOS_Error Create(uint8_t disk_num, fs::path path, const kiv_fat::Boot_Record& bpb, uint16_t* fat_table,
    const kiv_fat::Dir_Entry& parent_dir, const kiv_os::NFile_Attributes attributes) {
    kiv_fat::Dir_Entry file;
    file.attrs = static_cast<uint8_t>(attributes);
    std::string file_name = getFileName(path.filename());
    std::memcpy(file.name, file_name.c_str(), FILE_NAME_LENGTH);

    return kiv_fat::Create_File(disk_num, bpb, fat_table, parent_dir, file);
}

kiv_os::NOS_Error Fat_FS::Open(fs::path path, std::unique_ptr <Stream> &stream, const kiv_os::NOpen_File open_type,
                               const kiv_os::NFile_Attributes attributes) {
    std::lock_guard<std::mutex> lock(fat_fs_mutex);

    kiv_fat::Boot_Record bpb;
    std::vector <uint16_t> fat_table;

    kiv_os::NOS_Error status = kiv_fat::Load(diskNum, mParams, bpb, fat_table);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    kiv_fat::Dir_Entry file;
    kiv_fat::Dir_Entry parent_dir;
    bool last = false;

    status = kiv_fat::Find_File(diskNum, bpb, fat_table.data(), path, file, parent_dir, &last);
    if (status == kiv_os::NOS_Error::File_Not_Found) {
        if (open_type == kiv_os::NOpen_File::fmOpen_Always || !last) {
            return status;
        }

        file.attrs = static_cast<uint8_t>(attributes);
        std::string file_name = getFileName(path.filename());
        std::memcpy(file.name, file_name.c_str(), FILE_NAME_LENGTH);
        status = kiv_fat::Create_File(diskNum, bpb, fat_table.data(), parent_dir, file);
    }
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    if (isDirectory(file) != isDirectory(attributes)) {
        return kiv_os::NOS_Error::Invalid_Argument;
    }

    if (isReadOnly(file) && !isReadOnly(attributes)) {
        return kiv_os::NOS_Error::Permission_Denied;
    } else if (isReadOnly(attributes)) {
        file.attrs |= static_cast<uint8_t>(kiv_os::NFile_Attributes::Read_Only);
    }

    if (isDirectory(file)) {
        std::vector <kiv_os::TDir_Entry> entries;
        status = Read_Dir(diskNum, bpb, fat_table.data(), file, entries);
        if (status != kiv_os::NOS_Error::Success) {
            return status;
        }
        stream = std::make_unique<Fat_Dir_Stream>(entries);
    } else {
        stream = std::make_unique<Fat_Stream>(diskNum, mParams, path, file.size, file.attrs, *this);
    }

    return kiv_os::NOS_Error::Success;
}

kiv_os::NOS_Error Fat_FS::Set_File_Attributes(fs::path path, const kiv_os::NFile_Attributes attributes) {
    std::lock_guard<std::mutex> lock(fat_fs_mutex);

    kiv_fat::Boot_Record bpb;
    std::vector <uint16_t> fat_table;
    kiv_os::NOS_Error status = kiv_fat::Load(diskNum, mParams, bpb, fat_table);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    kiv_fat::Dir_Entry file;
    kiv_fat::Dir_Entry parent_dir;
    status = kiv_fat::Find_File(diskNum, bpb, fat_table.data(), path, file, parent_dir);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    file.attrs = static_cast<uint8_t>(attributes);

    return kiv_fat::Update_File(diskNum, bpb, fat_table.data(), parent_dir, file, false, false);
}

kiv_os::NOS_Error Fat_FS::Get_File_Attributes(fs::path path, kiv_os::NFile_Attributes &attributes) {
    std::lock_guard<std::mutex> lock(fat_fs_mutex);

    kiv_fat::Boot_Record bpb;
    std::vector <uint16_t> fat_table;
    kiv_os::NOS_Error status = kiv_fat::Load(diskNum, mParams, bpb, fat_table);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    kiv_fat::Dir_Entry file;
    kiv_fat::Dir_Entry parent_dir;
    status = kiv_fat::Find_File(diskNum, bpb, fat_table.data(), path, file, parent_dir);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    attributes = static_cast<kiv_os::NFile_Attributes>(file.attrs);
    return kiv_os::NOS_Error::Success;
}

bool isDirEmpty(const std::vector<kiv_fat::Dir_Entry> items) {
    for(auto &item : items) {
        if(item.name[0] == '\0') {
            continue;
        }

        if(strncmp(item.name, SELF_NAME, FILE_NAME_LENGTH) == 0) {
            continue;
        }

        if(strncmp(item.name, PARENT_NAME, FILE_NAME_LENGTH) == 0) {
            continue;
        }

        return false;
    }

    return true;
}

kiv_os::NOS_Error Fat_FS::Remove(fs::path path) {
    std::lock_guard<std::mutex> lock(fat_fs_mutex);

    kiv_fat::Boot_Record bpb;
    std::vector <uint16_t> fat_table;
    kiv_os::NOS_Error status = kiv_fat::Load(diskNum, mParams, bpb, fat_table);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    kiv_fat::Dir_Entry file;
    kiv_fat::Dir_Entry parent_dir;
    status = kiv_fat::Find_File(diskNum, bpb, fat_table.data(), path, file, parent_dir);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    if (file.attrs & static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory)) {
        std::vector <kiv_fat::Dir_Entry> items;
        status = kiv_fat::Read_Dir(diskNum, bpb, fat_table.data(), file, items);
        if (status != kiv_os::NOS_Error::Success) {
            return status;
        } else if (!isDirEmpty(items)) {
            return kiv_os::NOS_Error::Directory_Not_Empty;
        }
    }

    return kiv_fat::Delete_File(diskNum, bpb, fat_table.data(), parent_dir, file);
}

kiv_os::NOS_Error Fat_FS::Fat_Stream::Write(const void *buffer, const size_t size, size_t &written) {
    std::lock_guard<decltype(mutex)> lck(mutex);
    std::lock_guard<std::mutex> lock(fs.fat_fs_mutex);

    if (Is_Terminated()) {
        written = 0;
        return kiv_os::NOS_Error::Success;
    }

    kiv_fat::Boot_Record bpb;
    std::vector <uint16_t> fat_table;
    kiv_os::NOS_Error status = kiv_fat::Load(diskNum, mParams, bpb, fat_table);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    kiv_fat::Dir_Entry file;
    kiv_fat::Dir_Entry parent_dir;
    status = kiv_fat::Find_File(diskNum, bpb, fat_table.data(), mPath, file, parent_dir);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    written = 0;
    status = kiv_fat::Write_File(diskNum, bpb, fat_table.data(), file, (const char *) buffer, size, written, mPosition);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }
    mPosition += written;

    return kiv_fat::Update_File(diskNum, bpb, fat_table.data(), parent_dir, file, false, false);
}

kiv_os::NOS_Error Fat_FS::Fat_Stream::Read(void *buffer, const size_t size, size_t &read) {
    std::lock_guard<decltype(mutex)> lck(mutex);
    std::lock_guard<std::mutex> lock(fs.fat_fs_mutex);

    if (Is_Terminated()) {
        read = 0;
        return kiv_os::NOS_Error::Success;
    }

    kiv_fat::Boot_Record bpb;
    std::vector <uint16_t> fat_table;
    kiv_os::NOS_Error status = kiv_fat::Load(diskNum, mParams, bpb, fat_table);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    kiv_fat::Dir_Entry file;
    kiv_fat::Dir_Entry parent_dir;
    status = kiv_fat::Find_File(diskNum, bpb, fat_table.data(), mPath, file, parent_dir);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    read = 0;
    status = kiv_fat::Read_File(diskNum, bpb, fat_table.data(), file, (char *) buffer, size, read, mPosition);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }
    mPosition += read;

    return kiv_os::NOS_Error::Success;
}

kiv_os::NOS_Error Fat_FS::Fat_Stream::Seek(const int64_t offset, const kiv_os::NFile_Seek whence) {
    std::lock_guard<decltype(mutex)> lck(mutex);
    std::lock_guard<std::mutex> lock(fs.fat_fs_mutex);

    if (Is_Terminated()) {
        return kiv_os::NOS_Error::Success;
    }

    switch (static_cast<kiv_os::NFile_Seek>(whence)) {
        case kiv_os::NFile_Seek::Beginning:
            mPosition = offset;
            break;
        case kiv_os::NFile_Seek::Current:
            mPosition += offset;
            break;
        case kiv_os::NFile_Seek::End:
            mPosition = mSize + offset;
            break;
        default:
            return kiv_os::NOS_Error::Invalid_Argument;
    }
    return kiv_os::NOS_Error::Success;
}

kiv_os::NOS_Error Fat_FS::Fat_Stream::Resize(const int64_t offset, const kiv_os::NFile_Seek whence) {
    std::lock_guard<decltype(mutex)> lck(mutex);
    std::lock_guard<std::mutex> lock(fs.fat_fs_mutex);

    if (Is_Terminated()) {
        return kiv_os::NOS_Error::Success;
    }

    kiv_fat::Boot_Record bpb;
    std::vector <uint16_t> fat_table;
    kiv_os::NOS_Error status = kiv_fat::Load(diskNum, mParams, bpb, fat_table);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    kiv_fat::Dir_Entry file;
    kiv_fat::Dir_Entry parent_dir;
    status = kiv_fat::Find_File(diskNum, bpb, fat_table.data(), mPath, file, parent_dir);
    if (status != kiv_os::NOS_Error::Success) {
        return status;
    }

    switch (static_cast<kiv_os::NFile_Seek>(whence)) {
        case kiv_os::NFile_Seek::Beginning:
            mSize = static_cast<uint32_t>(mPosition = offset);
            break;
        case kiv_os::NFile_Seek::Current:
            mSize = static_cast<uint32_t>(mPosition = mPosition + offset);
            break;
        case kiv_os::NFile_Seek::End:
            mSize = static_cast<uint32_t>(mPosition = mSize + offset);
            break;
        default:
            return kiv_os::NOS_Error::Invalid_Argument;
    }

    return kiv_fat::Resize_File(diskNum, bpb, fat_table.data(), parent_dir, file, mSize);
}

bool Fat_FS::Fat_Stream::Is_Writable() {
    std::lock_guard<decltype(mutex)> lck(mutex);
    return !isReadOnly(mAttributes);
}

bool Fat_FS::Fat_Stream::Is_Resizable() {
    std::lock_guard<decltype(mutex)> lck(mutex);
    return !isReadOnly(mAttributes);
}

void Fat_FS::Fat_Stream::Terminate() {
    std::lock_guard<decltype(mutex)> lck(mutex);
    terminated = true;
}

bool Fat_FS::Fat_Stream::Is_Terminated() {
    std::lock_guard<decltype(mutex)> lck(mutex);
    return terminated;
}
