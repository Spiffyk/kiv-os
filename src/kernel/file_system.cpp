#include "fatfs.h"
#include "krtl.h"
#include "process.h"

static std::unique_ptr <VFS> Init_FAT(uint8_t disk_num, kiv_hal::TDrive_Parameters params) {
    auto fat = std::make_unique<Fat_FS>(disk_num, params);
    kiv_os::NOS_Error status = fat->Init();
    if (status != kiv_os::NOS_Error::Success) {
        fat.reset();
    }
    return fat;
}

void File_System::Init() {
    file_systems['P'] = std::make_unique<Proc_FS>();

    char disk = 'C';
    kiv_hal::TDrive_Parameters params;

    for (int i = 0; i < 256; i++) {
        uint8_t disk_num = static_cast<uint8_t>(i);

        if (kiv_krtl::Get_Drive_Params(disk_num, params)) {
            auto fs = Init_FAT(disk_num, params);
            if (fs) {
                file_systems[disk] = std::move(fs);
                disk++;
            }
        }
    }
}

VFS *File_System::Get_File_System(fs::path path) {
    if (!path.has_root_name()) {
        return nullptr;
    }

    char disk = path.root_name().string().c_str()[0];

    if (file_systems.count(disk) != 0) {
        return file_systems[disk].get();
    }
    return nullptr;
}

kiv_os::NOS_Error File_System::Open(fs::path path, std::unique_ptr <Stream> &stream, const kiv_os::NOpen_File open_type,
                                    const kiv_os::NFile_Attributes attributes) {
    VFS *fs = Get_File_System(path);
    return fs ? fs->Open(path, stream, open_type, attributes) : kiv_os::NOS_Error::Unknown_Filesystem;
}

kiv_os::NOS_Error File_System::Set_File_Attributes(fs::path path, const kiv_os::NFile_Attributes attributes) {
    VFS *fs = Get_File_System(path);
    return fs ? fs->Set_File_Attributes(path, attributes) : kiv_os::NOS_Error::Unknown_Filesystem;
}

kiv_os::NOS_Error File_System::Get_File_Attributes(fs::path path, kiv_os::NFile_Attributes &attributes) {
    VFS *fs = Get_File_System(path);
    return fs ? fs->Get_File_Attributes(path, attributes) : kiv_os::NOS_Error::Unknown_Filesystem;
}

kiv_os::NOS_Error File_System::Remove(fs::path path) {
    VFS *fs = Get_File_System(path);
    return fs ? fs->Remove(path) : kiv_os::NOS_Error::Unknown_Filesystem;
}