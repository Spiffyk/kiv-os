#include "stream_console.h"

// CONSOLE INPUT STREAM

Console_In_Stream::Console_In_Stream() : terminated(false) {
	// nothing more to do
}

kiv_os::NOS_Error Console_In_Stream::Write(const void* buffer, const size_t size, size_t& written) {
	return kiv_os::NOS_Error::IO_Error; // console input stream is not writable
}

kiv_os::NOS_Error Console_In_Stream::Read(void* buffer, const size_t size, size_t& read) {
	std::lock_guard<decltype(mutex)> lck(mutex);
	kiv_hal::TRegisters regs;

	if (Is_Terminated()) {
		read = 0;
		return kiv_os::NOS_Error::Success;
	}

	char* cbuffer = reinterpret_cast<char*>(buffer);
	size_t pos = 0;
	while (pos < size) {
		regs.rax.h = static_cast<decltype(regs.rax.l)>(kiv_hal::NKeyboard::Read_Char);
		kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::Keyboard, regs);

		if (!regs.flags.non_zero) {
			break; // nothing has been read
		}

		char ch = regs.rax.l;

		// known codes
		switch (static_cast<kiv_hal::NControl_Codes>(ch)) {
		case kiv_hal::NControl_Codes::BS:
			if (pos > 0) {
				pos--;
			}

			regs.rax.h = static_cast<decltype(regs.rax.l)>(kiv_hal::NVGA_BIOS::Write_Control_Char);
			regs.rdx.l = ch;
			kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::VGA_BIOS, regs);
			break;

		case kiv_hal::NControl_Codes::EOT:
			Terminate();
			[[fallthrough]];
		case kiv_hal::NControl_Codes::LF:
		case kiv_hal::NControl_Codes::NUL:
		case kiv_hal::NControl_Codes::CR:
			// reached end of line
			cbuffer[pos] = ch;
			pos++;
			read = pos;
			return kiv_os::NOS_Error::Success;


		default:
			cbuffer[pos] = ch;
			pos++;
			regs.rax.h = static_cast<decltype(regs.rax.l)>(kiv_hal::NVGA_BIOS::Write_String);
			regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(&ch);
			regs.rcx.r = 1;
			kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::VGA_BIOS, regs);
			break;
		}
	}

	read = pos;
	return kiv_os::NOS_Error::Success;
}

kiv_os::NOS_Error Console_In_Stream::Seek(const int64_t offset, const kiv_os::NFile_Seek whence) {
	return kiv_os::NOS_Error::IO_Error; // console streams are not seekable
}

kiv_os::NOS_Error Console_In_Stream::Resize(const int64_t offset, const kiv_os::NFile_Seek whence) {
	return kiv_os::NOS_Error::IO_Error; // console streams are not resizable
}

int64_t Console_In_Stream::Get_Seek() {
	return 0; // console streams are not seekable
}

bool Console_In_Stream::Is_Writable() {
	return false;
}

bool Console_In_Stream::Is_Readable() {
	return true;
}

bool Console_In_Stream::Is_Seekable() {
	return false;
}

bool Console_In_Stream::Is_Resizable() {
	return false;
}

void Console_In_Stream::Terminate() {
	std::lock_guard<decltype(mutex)> lck(mutex);
	terminated = true;
}

bool Console_In_Stream::Is_Terminated() {
	std::lock_guard<decltype(mutex)> lck(mutex);
	return terminated;
}



// CONSOLE OUTPUT STREAM

kiv_os::NOS_Error Console_Out_Stream::Write(const void* buffer, const size_t size, size_t& written) {
	std::lock_guard<decltype(mutex)> lck(mutex);

	if (Is_Terminated()) {
		written = 0;
		return kiv_os::NOS_Error::Success;
	}

	kiv_hal::TRegisters regs;
	regs.rax.h = static_cast<decltype(regs.rax.h)>(kiv_hal::NVGA_BIOS::Write_String);
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(buffer);
	regs.rcx.r = static_cast<decltype(regs.rcx.r)>(size);

	kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::VGA_BIOS, regs);

	if (regs.rax.r == 0) {
		written = 0;
		return kiv_os::NOS_Error::IO_Error;
	} else {
		written = size;
		return kiv_os::NOS_Error::Success;
	}
}

kiv_os::NOS_Error Console_Out_Stream::Read(void* buffer, const size_t size, size_t& read) {
	return kiv_os::NOS_Error::IO_Error; // console output stream is not readable
}

kiv_os::NOS_Error Console_Out_Stream::Seek(const int64_t offset, const kiv_os::NFile_Seek whence) {
	return kiv_os::NOS_Error::IO_Error; // console streams are not seekable
}

kiv_os::NOS_Error Console_Out_Stream::Resize(const int64_t offset, const kiv_os::NFile_Seek whence) {
	return kiv_os::NOS_Error::IO_Error; // console streams are not resizable
}

int64_t Console_Out_Stream::Get_Seek() {
	return 0; // console streams are not seekable
}

bool Console_Out_Stream::Is_Writable() {
	return true;
}

bool Console_Out_Stream::Is_Readable() {
	return false;
}

bool Console_Out_Stream::Is_Seekable() {
	return false;
}

bool Console_Out_Stream::Is_Resizable() {
	return false;
}

void Console_Out_Stream::Terminate() {
	std::lock_guard<decltype(mutex)> lck(mutex);
	terminated = true;
}

bool Console_Out_Stream::Is_Terminated() {
	std::lock_guard<decltype(mutex)> lck(mutex);
	return terminated;
}
