#pragma once

#include <Windows.h>

#include "kernel.h"
#include "io.h"
#include "process.h"

#include "krtl.h"

HMODULE User_Programs;

File_System *fileSystem = new File_System();

void Initialize_Kernel() {
	Initialize_IO();
	User_Programs = LoadLibraryW(L"user.dll");
	fileSystem->Init();
}

void Shutdown_Kernel() {
	Terminate_IO();
	Terminate_All_Processes();
	FreeLibrary(User_Programs);
	delete fileSystem;
}

void __stdcall Sys_Call(kiv_hal::TRegisters &regs) {
	Reset_Error(regs);
	switch (static_cast<kiv_os::NOS_Service_Major>(regs.rax.h)) {
		case kiv_os::NOS_Service_Major::File_System:
			Handle_IO(regs);
			break;
		case kiv_os::NOS_Service_Major::Process:
			Handle_Process(regs);
			break;
		default:
			Set_Error(regs, kiv_os::NOS_Error::Invalid_Argument);
	}
}

void __stdcall Bootstrap_Loader(kiv_hal::TRegisters &context) {
	Initialize_Kernel();
	kiv_hal::Set_Interrupt_Handler(kiv_os::System_Int_Number, Sys_Call);

	auto shell_hnd = kiv_krtl::Clone_Create_Process("shell", "", Get_Console_In(), Get_Console_Out());
	kiv_krtl::Wait_For(&shell_hnd, 1);

	Debug_Log("init exited with " << kiv_krtl::Read_Exit_Code(shell_hnd));

	Shutdown_Kernel();
}


void Set_Error(kiv_hal::TRegisters &regs, kiv_os::NOS_Error error) {
	regs.flags.carry = true;
	regs.rax.x = static_cast<uint16_t>(error);
}

void Reset_Error(kiv_hal::TRegisters& regs) {
	regs.flags.carry = false;
}

File_System *Get_File_System() {
    return fileSystem;
}
