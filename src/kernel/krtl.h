#pragma once
#include "../api/api.h"

/*
* A kernel-level RTL 
*/
namespace kiv_krtl {

	kiv_os::THandle Clone_Create_Process(const char* proc_name, const char* args, const kiv_os::THandle in, const kiv_os::THandle out);

	kiv_os::THandle Wait_For(const kiv_os::THandle* handles, const uint64_t count);

	uint16_t Read_Exit_Code(const kiv_os::THandle handle);

    bool Get_Drive_Params(uint8_t disk_num, kiv_hal::TDrive_Parameters &params);

    bool Read_From_Disk(uint8_t disk_num, uint64_t first_sector, uint64_t count, char *buffer, kiv_hal::NDisk_Status &status);

    bool Write_To_Disk(uint8_t disk_num, uint64_t first_sector, uint64_t count, const char *buffer, kiv_hal::NDisk_Status &status);

}
