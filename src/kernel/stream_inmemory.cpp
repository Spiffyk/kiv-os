#include "stream_inmemory.h"

#include <memory>

Inmemory_Stream::Inmemory_Stream(size_t size, bool writable)
	: area(new char[size]), area_size(size), writable(writable)
{
	// nothing more to do
}

Inmemory_Stream::Inmemory_Stream(void* src_buffer, size_t size, bool writable)
	: Inmemory_Stream(size, writable)
{
	memcpy(area, src_buffer, size);
}

void* Inmemory_Stream::Get_Area() {
	return this->area;
}

size_t Inmemory_Stream::Get_Area_Size() const {
	return this->area_size;
}

kiv_os::NOS_Error Inmemory_Stream::Write(const void* buffer, const size_t size, size_t& written) {
	std::lock_guard<decltype(mutex)> lck(mutex);

	if (!this->writable) {
		return kiv_os::NOS_Error::IO_Error;
	}

	if (position >= area_size || Is_Terminated()) {
		written = 0;
		return kiv_os::NOS_Error::Success;
	}

	size_t remaining = area_size - position;
	size_t to_write = (size > remaining) ? remaining : size;

	memcpy(reinterpret_cast<void*>(reinterpret_cast<size_t>(area) + position), buffer, to_write);
	position += to_write;
	written = to_write;

	return kiv_os::NOS_Error::Success;
}

kiv_os::NOS_Error Inmemory_Stream::Read(void* buffer, const size_t size, size_t& read) {
	std::lock_guard<decltype(mutex)> lck(mutex);

	if (position >= area_size || Is_Terminated()) {
		read = 0;
		return kiv_os::NOS_Error::Success;
	}

	size_t remaining = area_size - position;
	size_t to_read = (size > remaining) ? remaining : size;

	memcpy(buffer, reinterpret_cast<void*>(reinterpret_cast<size_t>(area) + position), to_read);
	position += to_read;
	read = to_read;

	return kiv_os::NOS_Error::Success;
}

kiv_os::NOS_Error Inmemory_Stream::Seek(const int64_t offset, const kiv_os::NFile_Seek whence) {
	std::lock_guard<decltype(mutex)> lck(mutex);

	if (Is_Terminated()) {
		return kiv_os::NOS_Error::Success;
	}

	int64_t result;
	switch (whence) {
	case kiv_os::NFile_Seek::Beginning:
		result = offset;
		break;
	case kiv_os::NFile_Seek::Current:
		result = position + offset;
		break;
	case kiv_os::NFile_Seek::End:
		result = area_size + offset;
		break;
	default:
		return kiv_os::NOS_Error::Invalid_Argument;
	}

	if (result < 0) {
		position = 0;
	} else if (result > static_cast<int64_t>(area_size)) {
		position = area_size;
	} else {
		position = result;
	}

	return kiv_os::NOS_Error::Success;
}

kiv_os::NOS_Error Inmemory_Stream::Resize(const int64_t offset, const kiv_os::NFile_Seek whence) {
	return kiv_os::NOS_Error::IO_Error; // an in-memory stream is not resizable
}

int64_t Inmemory_Stream::Get_Seek() {
	return position;
}

bool Inmemory_Stream::Is_Writable() {
	return this->writable;
}

bool Inmemory_Stream::Is_Readable() {
	return true;
}

bool Inmemory_Stream::Is_Seekable() {
	return true;
}

bool Inmemory_Stream::Is_Resizable() {
	return false;
}

void Inmemory_Stream::Terminate() {
	std::lock_guard<decltype(mutex)> lck(mutex);
	terminated = true;
}

bool Inmemory_Stream::Is_Terminated() {
	std::lock_guard<decltype(mutex)> lck(mutex);
	return terminated;
}

Inmemory_Stream::~Inmemory_Stream() {
	delete area;
}
