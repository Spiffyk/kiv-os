#include "stream_pipe.h"

bool Pipe::Is_Empty_And_Terminated() {
	std::lock_guard<decltype(mutex)> lck(mutex);
	return !filled_data && Is_Terminated();
}

Pipe::Pipe(const size_t buffer_size)
	: pipe_buffer(new char[buffer_size]),
	buffer_size(buffer_size),
	data_start(0),
	filled_data(0),
	sem_filled(0),
	sem_empty(buffer_size),
	terminated(false)
{
	// nothing more to do
}

kiv_os::NOS_Error Pipe::Write(const void* buffer, const size_t size, size_t& written) {
	const char* cbuffer = static_cast<const char*>(buffer);
	written = 0;

	if (Is_Terminated()) {
		return kiv_os::NOS_Error::Success;
	}

	for (size_t i = 0; i < size; ++i) {
		if (--sem_empty) {
			std::lock_guard<decltype(mutex)> lck(mutex);
			pipe_buffer[(data_start + filled_data) % buffer_size] = cbuffer[i];
			++filled_data;
			++written;
			++sem_filled;
		} else {
			if (Is_Terminated()) {
				break;
			} else {
				--i;
			}
		}
	}
	return kiv_os::NOS_Error::Success;
}

kiv_os::NOS_Error Pipe::Read(void* buffer, const size_t size, size_t& read) {
	char* cbuffer = static_cast<char*>(buffer);
	read = 0;

	if (Is_Empty_And_Terminated()) {
		return kiv_os::NOS_Error::Success;
	}

	for (size_t i = 0; i < size; ++i) {
		if (--sem_filled) {
			std::lock_guard<decltype(mutex)> lck(mutex);
			cbuffer[i] = pipe_buffer[data_start];
			--filled_data;
			data_start = (data_start + 1) % buffer_size;
			++read;
			++sem_empty;
		} else {
			if (Is_Terminated()) {
				break;
			} else {
				--i;
			}
		}
	}
	return kiv_os::NOS_Error::Success;
}

kiv_os::NOS_Error Pipe::Seek(const int64_t offset, const kiv_os::NFile_Seek whence) {
	return kiv_os::NOS_Error::IO_Error; // pipes are not seekable
}

kiv_os::NOS_Error Pipe::Resize(const int64_t offset, const kiv_os::NFile_Seek whence) {
	return kiv_os::NOS_Error::IO_Error; // pipes are not resizable
}

int64_t Pipe::Get_Seek() {
	return 0; // pipes are not seekable
}

bool Pipe::Is_Writable() {
	return true;
}

bool Pipe::Is_Readable() {
	return true;
}

bool Pipe::Is_Seekable() {
	return false;
}

bool Pipe::Is_Resizable() {
	return false;
}

void Pipe::Terminate() {
	std::lock_guard<decltype(term_mutex)> lck(term_mutex);
	terminated = true;
	sem_filled.Terminate();
	sem_empty.Terminate();
}

bool Pipe::Is_Terminated() {
	std::lock_guard<decltype(term_mutex)> lck(term_mutex);
	return terminated;
}

Pipe::~Pipe() {
	delete pipe_buffer;
}
