#include <cstdarg>
#include <cstring>

#include "kernel.h"

#include "krtl.h"



namespace kiv_krtl {

	kiv_hal::TRegisters Prepare_SysCall_Context(const kiv_os::NOS_Service_Major major, const uint8_t minor) {
		kiv_hal::TRegisters regs;
		regs.rax.h = static_cast<uint8_t>(major);
		regs.rax.l = minor;
		return regs;
	}
	
	kiv_os::THandle Clone_Create_Process(const char* proc_name, const char* args, const kiv_os::THandle in, const kiv_os::THandle out) {
		kiv_hal::TRegisters regs = Prepare_SysCall_Context(
			kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Clone));

		regs.rcx.r = static_cast<uint64_t>(kiv_os::NClone::Create_Process);
		regs.rdx.r = reinterpret_cast<uint64_t>(proc_name);
		regs.rdi.r = reinterpret_cast<uint64_t>(args);
		regs.rbx.e = (in << 16) | out;

		Sys_Call(regs);
		return static_cast<kiv_os::THandle>(regs.rax.x);
	}

	kiv_os::THandle Wait_For(const kiv_os::THandle* handles, const uint64_t count) {
		kiv_hal::TRegisters regs = Prepare_SysCall_Context(
			kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Wait_For));
		regs.rdx.r = reinterpret_cast<uint64_t>(handles);
		regs.rcx.r = count;
		Sys_Call(regs);
		return handles[regs.rax.r];
	}

	uint16_t Read_Exit_Code(const kiv_os::THandle handle) {
		kiv_hal::TRegisters regs = Prepare_SysCall_Context(
			kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Read_Exit_Code));

		regs.rdx.x = handle;

		Sys_Call(regs);
		return regs.rcx.x;
	}

	kiv_hal::TDisk_Address_Packet Prepare_Disk_Address_Packet(uint64_t first_sector, uint64_t count, char *buffer) {
	    kiv_hal::TDisk_Address_Packet addressPacket;
	    addressPacket.lba_index = first_sector;
	    addressPacket.count = count;
	    addressPacket.sectors = buffer;
		return addressPacket;
	}

    bool Get_Drive_Params(uint8_t disk_num, kiv_hal::TDrive_Parameters &params) {
        kiv_hal::TRegisters regs;
        regs.rax.h = static_cast<uint8_t>(kiv_hal::NDisk_IO::Drive_Parameters);
        regs.rdi.r = reinterpret_cast<uint64_t>(&params);
        regs.rdx.l = disk_num;

        kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::Disk_IO, regs);
        return !regs.flags.carry;
	}

    bool Read_From_Disk(uint8_t disk_num, uint64_t first_sector, uint64_t count, char *buffer, kiv_hal::NDisk_Status &status) {
        kiv_hal::TDisk_Address_Packet addressPacket = Prepare_Disk_Address_Packet(first_sector, count, buffer);
        kiv_hal::TRegisters regs;

        regs.rax.h = static_cast<uint8_t>(kiv_hal::NDisk_IO::Read_Sectors);
        regs.rdi.r = reinterpret_cast<uint64_t>(&addressPacket);
        regs.rdx.l = disk_num;

        kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::Disk_IO, regs);
        status = static_cast<kiv_hal::NDisk_Status>(regs.rax.x);
        return !regs.flags.carry;
	}

    bool Write_To_Disk(uint8_t disk_num, uint64_t first_sector, uint64_t count, const char *buffer, kiv_hal::NDisk_Status &status) {
        kiv_hal::TDisk_Address_Packet addressPacket = Prepare_Disk_Address_Packet(first_sector, count, const_cast<char*>(buffer));
        kiv_hal::TRegisters regs;

        regs.rax.h = static_cast<uint8_t>(kiv_hal::NDisk_IO::Write_Sectors);
        regs.rdi.r = reinterpret_cast<uint64_t>(&addressPacket);
        regs.rdx.l = disk_num;

        kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::Disk_IO, regs);
        status = static_cast<kiv_hal::NDisk_Status>(regs.rax.x);
        return !regs.flags.carry;
    }

}
