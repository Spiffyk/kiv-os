#pragma once
#include <mutex>
#include "stream.h"

class Console_In_Stream : public Stream {

	std::recursive_mutex mutex;
	bool terminated = false;

public:

	Console_In_Stream();
	
	kiv_os::NOS_Error Write(const void* buffer, const size_t size, size_t& written) override;

	kiv_os::NOS_Error Read(void* buffer, const size_t size, size_t& read) override;

	kiv_os::NOS_Error Seek(const int64_t offset, const kiv_os::NFile_Seek whence) override;

	kiv_os::NOS_Error Resize(const int64_t offset, const kiv_os::NFile_Seek whence) override;

	int64_t Get_Seek() override;

	bool Is_Writable() override;

	bool Is_Readable() override;

	bool Is_Seekable() override;

	bool Is_Resizable() override;

	void Terminate() override;

	bool Is_Terminated() override;
};


class Console_Out_Stream : public Stream {

	std::recursive_mutex mutex;
	bool terminated = false;

public:

	kiv_os::NOS_Error Write(const void* buffer, const size_t size, size_t& written) override;

	kiv_os::NOS_Error Read(void* buffer, const size_t size, size_t& read) override;

	kiv_os::NOS_Error Seek(const int64_t offset, const kiv_os::NFile_Seek whence) override;

	kiv_os::NOS_Error Resize(const int64_t offset, const kiv_os::NFile_Seek whence) override;

	int64_t Get_Seek() override;

	bool Is_Writable() override;

	bool Is_Readable() override;

	bool Is_Seekable() override;

	bool Is_Resizable() override;

	void Terminate() override;

	bool Is_Terminated() override;
};
