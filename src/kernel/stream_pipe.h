#pragma once

#include <mutex>
#include "semaphore.h"
#include "Stream.h"

/**
* A producer-consumer stream. Any data that is written into this stream using the Write method may subsequently be
* read using the Read method.
*/
class Pipe : public Stream {

	char* pipe_buffer;
	const size_t buffer_size;

	bool terminated = false;
	std::recursive_mutex term_mutex;

	size_t data_start;
	size_t filled_data;
	Semaphore<size_t> sem_filled;
	Semaphore<size_t> sem_empty;
	std::recursive_mutex mutex;

	bool Is_Empty_And_Terminated();

public:

	/**
	* Creates a new pipe with a buffer of the specified size.
	*/
	Pipe(const size_t buffer_size = (1024 * 1024));

	kiv_os::NOS_Error Write(const void* buffer, const size_t size, size_t& written) override;

	kiv_os::NOS_Error Read(void* buffer, const size_t size, size_t& read) override;

	kiv_os::NOS_Error Seek(const int64_t offset, const kiv_os::NFile_Seek whence) override;

	kiv_os::NOS_Error Resize(const int64_t offset, const kiv_os::NFile_Seek whence) override;

	int64_t Get_Seek() override;

	bool Is_Writable() override;

	bool Is_Readable() override;

	bool Is_Seekable() override;

	bool Is_Resizable() override;

	void Terminate() override;

	bool Is_Terminated() override;

	/**
	* Closes the pipe.
	*/
	virtual ~Pipe();

};
