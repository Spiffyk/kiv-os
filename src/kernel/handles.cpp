#include "handles.h"

#include <map>
#include <mutex>
#include <random>
#include <set>
#include <condition_variable>

#undef max

struct Listener {
	kiv_hnd::CvHolder& cvh;
	uint64_t index;

	Listener(kiv_hnd::CvHolder& h, uint64_t i) : cvh(h), index(i) {}
};

std::set<kiv_os::THandle> Allocated_Handles;
std::recursive_mutex Handles_Guard;

std::map<kiv_os::THandle, HANDLE> Handles_To_Natives;
std::map<HANDLE, kiv_os::THandle> Natives_To_Handles;
std::map<kiv_os::THandle, std::thread::id> Handles_To_Threads;
std::map<std::thread::id, kiv_os::THandle> Threads_To_Handles;
std::recursive_mutex Handle_Maps_Guard;

std::set<kiv_os::THandle> Signalled_Handles;
std::map<kiv_os::THandle, std::map<kiv_os::THandle, Listener>> Handles_To_Listeners;
std::mutex Listeners_Guard;


kiv_os::THandle Last_Handle = 0;

std::random_device rd;
std::mt19937 gen(rd());
std::uniform_int_distribution<> dis(1, 6);


kiv_os::THandle Alloc_Handle() {
	std::lock_guard<std::recursive_mutex> guard(Handles_Guard);

	if (Allocated_Handles.size() >= std::numeric_limits<kiv_os::THandle>::max()) {
		return kiv_os::Invalid_Handle;
	}

	do {
		Last_Handle += dis(gen);
	} while (Allocated_Handles.find(Last_Handle) != Allocated_Handles.end());

	Allocated_Handles.insert(Last_Handle);
	return Last_Handle;
}

bool Remove_Handle(const kiv_os::THandle hnd) {
	std::lock_guard<std::recursive_mutex> guard_a(Handles_Guard);
	std::lock_guard<std::recursive_mutex> guard_b(Handle_Maps_Guard);
	std::lock_guard<std::mutex> guard_c(Listeners_Guard);

	Handles_To_Listeners.erase(hnd);
	Signalled_Handles.erase(hnd);

	{
		auto resolved = Handles_To_Natives.find(hnd);
		if (resolved != Handles_To_Natives.end()) {
			Handles_To_Natives.erase(resolved);
			Natives_To_Handles.erase(resolved->second);
		}
	}

	{
		auto resolved = Handles_To_Threads.find(hnd);
		if (resolved != Handles_To_Threads.end()) {
			Handles_To_Threads.erase(resolved);
			Threads_To_Handles.erase(resolved->second);
		}
	}

	return Allocated_Handles.erase(hnd) == 1;
}

kiv_os::THandle To_kiv_os_Handle(const HANDLE hnd) {
	std::lock_guard<std::recursive_mutex> guard(Handle_Maps_Guard);
	
	auto resolved = Natives_To_Handles.find(hnd);
	if (resolved != Natives_To_Handles.end()) {
		return resolved->second;
	}

	return kiv_os::Invalid_Handle;
}

kiv_os::THandle To_kiv_os_Handle(const std::thread::id hnd) {
	std::lock_guard<std::recursive_mutex> guard(Handle_Maps_Guard);

	auto resolved = Threads_To_Handles.find(hnd);
	if (resolved != Threads_To_Handles.end()) {
		return resolved->second;
	}

	return kiv_os::Invalid_Handle;
}


kiv_os::THandle To_kiv_os_Handle_Alloc(const HANDLE hnd) {
	std::lock_guard<std::recursive_mutex> guard(Handle_Maps_Guard);

	auto handle = To_kiv_os_Handle(hnd);
	if (handle != kiv_os::Invalid_Handle) {
		return handle;
	}

	auto new_handle = Alloc_Handle();
	Assign_Native_Handle(new_handle, hnd);

	return new_handle;
}

void Assign_Native_Handle(const kiv_os::THandle kivos, const HANDLE native) {
	std::lock_guard<std::recursive_mutex> guard(Handle_Maps_Guard);
	Handles_To_Natives[kivos] = native;
	Natives_To_Handles[native] = kivos;
}

void Assign_Thread_Handle(const kiv_os::THandle kivos, const std::thread::id thread) {
	std::lock_guard<std::recursive_mutex> guard(Handle_Maps_Guard);
	Handles_To_Threads[kivos] = thread;
	Threads_To_Handles[thread] = kivos;
}

HANDLE To_Native_Handle(const kiv_os::THandle hnd) {
	std::lock_guard<std::recursive_mutex> guard(Handle_Maps_Guard);

	auto resolved = Handles_To_Natives.find(hnd);
	if (resolved == Handles_To_Natives.end()) {
		return INVALID_HANDLE_VALUE;
		
	}
	
	return resolved->second;
}

std::thread::id To_Thread_Id(const kiv_os::THandle hnd) {
	std::lock_guard<std::recursive_mutex> guard(Handle_Maps_Guard);

	auto resolved = Handles_To_Threads.find(hnd);
	if (resolved == Handles_To_Threads.end()) {
		return std::thread::id();
	}

	return resolved->second;
}

bool Add_Handle_Listener(
	const kiv_os::THandle hnd,
	const uint64_t index,
	const kiv_os::THandle holder_hnd,
	kiv_hnd::CvHolder& holder)
{
	std::lock_guard<std::mutex> guard_c(Listeners_Guard);

	if (Allocated_Handles.find(hnd) == Allocated_Handles.end() || Signalled_Handles.find(hnd) != Signalled_Handles.end()) {
		{
			std::lock_guard<std::mutex> guard(holder.mutex);
			holder.index = index;
			holder.signalled = true;
		}
		holder.cv.notify_all();
		return false;
	}

	auto l = Handles_To_Listeners.emplace(hnd, std::map<kiv_os::THandle, Listener>());
	l.first->second.emplace(holder_hnd, Listener(holder, index));
	return true;
}

void Remove_Handle_Listener(const kiv_os::THandle hnd, const kiv_os::THandle holder_hnd) {
	std::lock_guard<std::mutex> guard_c(Listeners_Guard);

	auto it = Handles_To_Listeners.find(hnd);
	if (it == Handles_To_Listeners.end()) {
		return;
	}

	auto& lmap = it->second;
	lmap.erase(holder_hnd);
	if (lmap.empty()) {
		Handles_To_Listeners.erase(it);
	}
}

void Signal_Handle(const kiv_os::THandle hnd) {
	std::lock_guard<std::mutex> guard_c(Listeners_Guard);

	if (Allocated_Handles.find(hnd) == Allocated_Handles.end()) {
		return;
	}

	Signalled_Handles.insert(hnd);

	auto it = Handles_To_Listeners.find(hnd);
	if (it == Handles_To_Listeners.end()) {
		return;
	}

	auto& lmap = it->second;
	std::for_each(lmap.begin(), lmap.end(), [](auto& p) {
		auto& listener = p.second;
		{
			std::lock_guard<std::mutex> guard_cv(listener.cvh.mutex);

			listener.cvh.index = listener.index;
			listener.cvh.signalled = true;
		}
		listener.cvh.cv.notify_all();
	});
}
