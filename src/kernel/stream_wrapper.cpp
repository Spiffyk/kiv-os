#include "kernel.h"
#include "stream_wrapper.h"

Wrapper_Stream::Wrapper_Stream(std::shared_ptr<Stream>&& to_wrap, const bool allow_read, const bool allow_write)
		: backend(to_wrap), read_allowed(allow_read), write_allowed(allow_write)
{
	// nothing more to do
}

kiv_os::NOS_Error Wrapper_Stream::Write(const void* buffer, const size_t size, size_t& written) {
	if (!write_allowed) {
		Debug_Log("Attempted to write into a stream wrapper with disabled write");
		return kiv_os::NOS_Error::Permission_Denied;
	}

	return backend->Write(buffer, size, written);
}

kiv_os::NOS_Error Wrapper_Stream::Read(void* buffer, const size_t size, size_t& read) {
	if (!read_allowed) {
		Debug_Log("Attempted to read from a stream wrapper with disabled read");
		return kiv_os::NOS_Error::Permission_Denied;
	}

	return backend->Read(buffer, size, read);
}

kiv_os::NOS_Error Wrapper_Stream::Seek(const int64_t offset, const kiv_os::NFile_Seek whence) {
	return backend->Seek(offset, whence);
}

kiv_os::NOS_Error Wrapper_Stream::Resize(const int64_t offset, const kiv_os::NFile_Seek whence) {
	return backend->Resize(offset, whence);
}

int64_t Wrapper_Stream::Get_Seek() {
	return backend->Get_Seek();
}

bool Wrapper_Stream::Is_Writable() {
	return write_allowed && backend->Is_Writable();
}

bool Wrapper_Stream::Is_Readable() {
	return read_allowed && backend->Is_Readable();
}

bool Wrapper_Stream::Is_Seekable() {
	return backend->Is_Seekable();
}

bool Wrapper_Stream::Is_Resizable() {
	return backend->Is_Resizable();
}

void Wrapper_Stream::Terminate() {
	backend->Terminate();
}

bool Wrapper_Stream::Is_Terminated() {
	return backend->Is_Terminated();
}

Wrapper_Stream::~Wrapper_Stream() {
	backend->Terminate();
}
