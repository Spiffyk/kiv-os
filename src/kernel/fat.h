#ifndef KIV_OS_FAT_H
#define KIV_OS_FAT_H

#include "../api/api.h"
#include <vector>
#include <filesystem>
#include <cstdio>

namespace fs = std::filesystem;

#define BOOT_SECTOR_OFFSET 11
#define MAX_CHAIN_SIZE 1024
#define DATA_CLUSTER_OFFSET 2
#define FILE_NAME_LENGTH 11

namespace kiv_fat {

#pragma pack(push, 1)
    struct Boot_Record {
        uint16_t bytes_per_sector;
        uint8_t sectors_per_cluster;
        uint16_t reserved_sectors;
        uint8_t num_of_fats;
        uint16_t max_root_entries;
        uint16_t total_sectors;
        uint8_t ignore;
        uint16_t sectors_per_fat;
        uint16_t sectors_per_track;
        uint16_t heads_count;
        char reserved[10];
        uint8_t boot_signature;
        uint32_t volume_id;
        char volume_label[11];
    };
#pragma pack(pop)


#pragma pack(push, 1)
    struct Dir_Entry {
        char name[FILE_NAME_LENGTH];
        uint8_t attrs; // NFile_Attributes
        char reserved[10];
        uint16_t write_time;
        uint16_t write_date;
        uint16_t first_cluster;
        uint32_t size;
    };
#pragma pack(pop)

    struct Fat_Chain {
        uint16_t start;
        uint32_t size;
    };

    /**
     * Load boot record and fat table
     */
    kiv_os::NOS_Error Load(uint8_t disk_num, kiv_hal::TDrive_Parameters &params,
                           Boot_Record &bpb, std::vector <uint16_t> &fat_table);

    /**
     * Save fat table(s)
     */
    kiv_os::NOS_Error Update(uint8_t disk_num, const Boot_Record &bpb, const uint16_t *fat_table);

    /**
     * Read file to buffer
     */
    kiv_os::NOS_Error
    Read_File(uint8_t disk_num, const Boot_Record &bpb, const uint16_t *fat_table, const Dir_Entry &file,
              char *buffer, size_t buffer_size, size_t &read, uint64_t offset);

    /**
     * Read directory items to vector
     */
    kiv_os::NOS_Error
    Read_Dir(uint8_t disk_num, const Boot_Record &bpb, const uint16_t *fat_table, const Dir_Entry &dir,
             std::vector <Dir_Entry> &items);

    /**
     * Read root directory
     */
    kiv_os::NOS_Error Read_Root_Dir(uint8_t disk_num, const Boot_Record &bpb, std::vector <Dir_Entry> &items);

    /**
     * Write to existing file data from buffer
     */
    kiv_os::NOS_Error Write_File(uint8_t disk_num, const Boot_Record &bpb, uint16_t *fat_table, Dir_Entry &file,
                                 const char *buffer, size_t buffer_size, size_t &written, uint64_t offset);

    /**
     * Create new file in parent_dir
     */
    kiv_os::NOS_Error Create_File(uint8_t disk_num, const Boot_Record &bpb, uint16_t *fat_table,
                                  const Dir_Entry &parent_dir, Dir_Entry &new_file);

    /**
     * Delete file
     */
    kiv_os::NOS_Error Delete_File(uint8_t disk_num, const Boot_Record &bpb, uint16_t *fat_table,
                                  const Dir_Entry &parent_dir, Dir_Entry &file);

    /**
     * Update file attrs
     * @param create if true - create new dir entry if not exists
     */
    kiv_os::NOS_Error Update_File(uint8_t disk_num, const Boot_Record &bpb, const uint16_t *fat_table,
                                  const Dir_Entry &parent_dir, Dir_Entry &file, bool create, bool remove);

    /**
     * Resize file
     */
    kiv_os::NOS_Error Resize_File(uint8_t disk_num, const Boot_Record &bpb, uint16_t *fat_table,
                                  const Dir_Entry &parent_dir, Dir_Entry &file, uint32_t new_size);

    /**
     * Find file and parent dir
     * @param last true if full path exists except last path part (filename)
     */
    kiv_os::NOS_Error Find_File(uint8_t disk_num, const Boot_Record &bpb, const uint16_t *fat_table,
                                fs::path &path, Dir_Entry &file, Dir_Entry &parent_dir, bool *last = nullptr);


}

#endif /* KIV_OS_FAT_H */
