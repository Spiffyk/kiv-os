#ifndef KIV_OS_FATFS_H
#define KIV_OS_FATFS_H

#include <memory>
#include <mutex>

#include "file_system.h"
#include "stream_inmemory.h"
#include "fat.h"

class Fat_FS : public VFS {
public:
    Fat_FS(uint8_t diskNum, kiv_hal::TDrive_Parameters params) : diskNum(diskNum), mParams(params) {}

    Fat_FS(const Fat_FS&) = delete;

    kiv_os::NOS_Error Init();

    kiv_os::NOS_Error Open(fs::path path, std::unique_ptr <Stream> &stream, const kiv_os::NOpen_File open_type,
                           const kiv_os::NFile_Attributes attributes) override;

    kiv_os::NOS_Error Set_File_Attributes(fs::path path, const kiv_os::NFile_Attributes attributes) override;

    kiv_os::NOS_Error Get_File_Attributes(fs::path path, kiv_os::NFile_Attributes &attributes) override;

    kiv_os::NOS_Error Remove(fs::path path) override;

private:
    class Fat_Stream;
    class Fat_Dir_Stream;

    uint8_t diskNum;
    kiv_hal::TDrive_Parameters mParams;
    std::mutex fat_fs_mutex;
};


class Fat_FS::Fat_Stream : public Stream {
public:
    Fat_Stream(uint8_t disk_num, kiv_hal::TDrive_Parameters params, fs::path path, uint32_t size, uint8_t attributes, Fat_FS &fs)
            : diskNum(disk_num), mParams(params), mPath(path), mSize(size), mAttributes(attributes), fs(fs) {}

    kiv_os::NOS_Error Write(const void *buffer, const size_t size, size_t &written) override;

    kiv_os::NOS_Error Read(void *buffer, const size_t size, size_t &read) override;

    kiv_os::NOS_Error Seek(const int64_t offset, const kiv_os::NFile_Seek whence) override;

    kiv_os::NOS_Error Resize(const int64_t offset, const kiv_os::NFile_Seek whence) override;

    int64_t Get_Seek() override { return mPosition; }

    bool Is_Writable() override;

    bool Is_Readable() override { return true; }

    bool Is_Seekable() override { return true; }

    bool Is_Resizable() override;

    void Terminate() override;

    bool Is_Terminated() override;

private:
    std::recursive_mutex mutex;
    uint8_t diskNum;
    kiv_hal::TDrive_Parameters mParams;
    int64_t mPosition{0};
    fs::path mPath;
    uint32_t mSize;
    uint8_t mAttributes;
    Fat_FS& fs;
    bool terminated = false;
};


class Fat_FS::Fat_Dir_Stream : public Inmemory_Stream {
public:

    Fat_Dir_Stream(const std::vector <kiv_os::TDir_Entry> &vector) : Inmemory_Stream(vector, false) {}

};

#endif /* KIV_OS_FATFS_H */