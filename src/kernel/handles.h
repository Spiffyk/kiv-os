#pragma once

#include "..\api\api.h"

#include <Windows.h>
#include <mutex>
#include <condition_variable>
#include <thread>


namespace kiv_hnd {
	
	struct CvHolder {
		static const uint64_t INVALID_INDEX = -1;

		std::mutex mutex;
		std::condition_variable cv;
		bool signalled = false;
		uint64_t index = INVALID_INDEX;
	};

}


// Allocates a new unique kiv_os handle.
kiv_os::THandle Alloc_Handle();

// Removes the specified handle and all of its associations.
bool Remove_Handle(const kiv_os::THandle hnd);

// Converts the specified native handle to a kiv_os handle. If no existing kiv_os handle is found, returns
// Invalid_Handle.
kiv_os::THandle To_kiv_os_Handle(const HANDLE hnd);

// Converts the specified thread id to a kiv_os handle. If no existing kiv_os handle is found, returns
// `std::thread::id()`.
kiv_os::THandle To_kiv_os_Handle(const std::thread::id hnd);

// Converts the specified native handle to a kiv_os handle. If no existing kiv_os handle is found, allocates a new one.
kiv_os::THandle To_kiv_os_Handle_Alloc(const HANDLE hnd);

// Assigns the specified native handle to the specified existing kiv_os handle.
void Assign_Native_Handle(const kiv_os::THandle kivos, const HANDLE native);

// Assigns the specified thread id to the specified existing kiv_os handle.
void Assign_Thread_Handle(const kiv_os::THandle kivos, const std::thread::id thread);

// If the specified kiv_os handle represents a native handle, returns the native handle, if not, returns
// `INVALID_HANDLE_VALUE`.
HANDLE To_Native_Handle(const kiv_os::THandle hnd);

// If the specified kiv_os handle represents a thread, returns the thread, if not, returns `std::thread::id()`.
std::thread::id To_Thread_Id(const kiv_os::THandle hnd);

bool Add_Handle_Listener(const kiv_os::THandle hnd, const uint64_t index, const kiv_os::THandle holder_hnd, kiv_hnd::CvHolder& holder);

void Remove_Handle_Listener(const kiv_os::THandle hnd, const kiv_os::THandle holder_hnd);

void Signal_Handle(const kiv_os::THandle hnd);
