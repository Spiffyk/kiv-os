#pragma once

#include "..\api\api.h"

void Terminate_Handle(kiv_os::THandle handle);

void Close_Handle(kiv_os::THandle handle);

void Handle_IO(kiv_hal::TRegisters &regs);

void Initialize_IO();

void Terminate_IO();

kiv_os::THandle Get_Console_In();

kiv_os::THandle Get_Console_Out();
