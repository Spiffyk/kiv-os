#ifndef KIV_OS_FILE_SYSTEM_H
#define KIV_OS_FILE_SYSTEM_H

#include "map"
#include "memory"
#include <filesystem>
#include "../api/api.h"
#include "Stream.h"

namespace fs = std::filesystem;

struct VFS {

    virtual kiv_os::NOS_Error Open(fs::path path, std::unique_ptr <Stream> &stream, const kiv_os::NOpen_File open_type,
                                   const kiv_os::NFile_Attributes attributes) = 0;

    virtual kiv_os::NOS_Error Set_File_Attributes(fs::path path, const kiv_os::NFile_Attributes attributes) = 0;

    virtual kiv_os::NOS_Error Get_File_Attributes(fs::path path, kiv_os::NFile_Attributes &attributes) = 0;

    virtual kiv_os::NOS_Error Remove(fs::path path) = 0;
};

class File_System : public VFS {
public:
    File_System() = default;

    void Init();

    kiv_os::NOS_Error Open(fs::path path, std::unique_ptr <Stream> &stream, const kiv_os::NOpen_File open_type,
                           const kiv_os::NFile_Attributes attributes) override;

    kiv_os::NOS_Error Set_File_Attributes(fs::path path, const kiv_os::NFile_Attributes attributes) override;

    kiv_os::NOS_Error Get_File_Attributes(fs::path path, kiv_os::NFile_Attributes &attributes) override;

    kiv_os::NOS_Error Remove(fs::path path) override;

private:
    std::map<char, std::unique_ptr<VFS>> file_systems;

    VFS *Get_File_System(fs::path path);
};

#endif /* KIV_OS_FILE_SYSTEM_H */
