#pragma once

#include <filesystem>
#include "..\api\api.h"
#include "Stream.h"
#include "file_system.h"

/**
* Assigns the specified handle to the current process, making it auto-close once the process exits.
*/
void Current_Process_Own_Handle(kiv_os::THandle handle);

/**
* Removes the specified handle's assignment to the current process. The process will no longer attempt to auto-close
* the handle once it exits.
*/
void Current_Process_Disown_Handle(kiv_os::THandle handle);

/**
* Checks if the current process is privileged to manipulate the specified handle. This is mostly important for closing.
* If this function is called from the kernel thread, it will always return `true`.
*/
bool Check_Current_Process_Owns_Handle(kiv_os::THandle handle);

/**
* Gets the working directory of the current process.
*/
fs::path Current_Process_Working_Dir();

/**
* Sets the working directory of the current process.
*/
void Set_Current_Process_Working_Dir(fs::path working_dir);

/**
* Handles a NOS_Process system call.
*/
void Handle_Process(kiv_hal::TRegisters& regs);

/**
* Correctly disposes of all running processes.
*/
void Terminate_All_Processes();


/**
* A filesystem supplying metadata about processes.
*/
class Proc_FS : public VFS {

	kiv_os::NOS_Error Open(fs::path path, std::unique_ptr <Stream> &stream, const kiv_os::NOpen_File open_type,
                                   const kiv_os::NFile_Attributes attributes) override;

    kiv_os::NOS_Error Set_File_Attributes(fs::path path, const kiv_os::NFile_Attributes attributes) override;

    kiv_os::NOS_Error Get_File_Attributes(fs::path path, kiv_os::NFile_Attributes &attributes) override;

    kiv_os::NOS_Error Remove(fs::path path) override;

};
