#include <algorithm>
#include "fat.h"
#include "krtl.h"

namespace kiv_fat {

    enum class Fat_Entry_Value : std::uint16_t {
        Unused = 0x00,
        ReservedStart = 0xFF0,
        ReservedEnd = 0xFF6,
        BadCluster = 0xFF7,
        EndOfFile = 0xFF8 // 0xFF8 - 0xFFF
    };

    inline bool Is_Unused(uint16_t cluster) { return cluster == static_cast<uint16_t>(Fat_Entry_Value::Unused); }

    inline bool Is_Reserved(uint16_t cluster) {
        return cluster >= static_cast<uint16_t>(Fat_Entry_Value::ReservedEnd) &&
               cluster <= static_cast<uint16_t>(Fat_Entry_Value::ReservedEnd);
    }

    inline bool Is_Bad_Cluster(uint16_t cluster) {
        return cluster == static_cast<uint16_t>(Fat_Entry_Value::BadCluster);
    }

    inline bool Is_End_Of_File(uint16_t cluster) {
        return cluster >= static_cast<uint16_t>(Fat_Entry_Value::EndOfFile);
    }

    inline size_t Bytes_Per_Cluster(Boot_Record Boot_Record) {
        return Boot_Record.bytes_per_sector * static_cast<size_t>(Boot_Record.sectors_per_cluster);
    }

    inline uint64_t First_Data_Sector(Boot_Record Boot_Record) {
        return static_cast<uint64_t>(Boot_Record.reserved_sectors) +
               Boot_Record.sectors_per_fat * static_cast<uint64_t>(Boot_Record.num_of_fats)
               + (Boot_Record.max_root_entries * sizeof(Dir_Entry)) / Boot_Record.bytes_per_sector -
               DATA_CLUSTER_OFFSET;
    }

    inline void Clear_Dir_Entry(Dir_Entry &entry) {
        std::memset(entry.name, 0, FILE_NAME_LENGTH);
        std::memset(entry.reserved, 0, 10);
        entry.attrs = 0;
        entry.size = 0;
        entry.first_cluster = 0;
        entry.write_time = 0;
        entry.write_date = 0;
    }

    uint16_t Get_Fat_Value(uint16_t cluster, const uint16_t *fat_table) {
        uint8_t *bytes = (uint8_t *) fat_table;
        uint16_t pos = (3 * cluster) / 2;
        uint16_t res = *((uint16_t * )(bytes + pos));

        if (cluster % 2 == 0) {
            res = res << 4;
            res = res >> 4;
        } else {
            res = res >> 4;
        }

        return res;
    }

    void Set_Fat_Value(uint16_t cluster, const uint16_t *fat_table, uint16_t value) {
        uint8_t *bytes = (uint8_t *) fat_table;
        uint16_t pos = (3 * cluster) / 2;
        uint16_t res = *((uint16_t * )(bytes + pos));

        if (cluster % 2 == 0) {
            res = res >> 12;
            res = res << 12;
            res = res | value;
        } else {
            res = res << 12;
            res = res >> 12;
            res = res | (value << 4);
        }

        *((uint16_t * )(bytes + pos)) = res;
    }

    size_t Get_Free_Clusters_Count(const Boot_Record bpb, const uint16_t *fat_table) {
        size_t count = 0;

        for (uint16_t i = 1; i < bpb.total_sectors; i++) {
            if (Is_Unused(Get_Fat_Value(i, fat_table))) {
                count++;
            }
        }

        return count;
    }

    inline uint16_t Get_Free_Cluster(const Boot_Record bpb, uint16_t *fat_table) {
        for (uint16_t i = 1; i < bpb.total_sectors; i++) {
            if (Is_Unused(Get_Fat_Value(i, fat_table))) {
                return i;
            }
        }

        return 0;
    }

    std::string Get_File_Name(fs::path path) {
        std::string p = path.string();
        std::string e = path.extension().string();

        p.resize(p.length() - e.length());
        if (path.has_extension()) {
            e.erase(0, 1);
        }
        p.resize(FILE_NAME_LENGTH - e.length(), 0x20);
        p.append(e);

        std::transform(p.begin(), p.end(), p.begin(), ::toupper);

        return p;
    }

    inline size_t Get_File_Clusters_Count(const uint16_t *fat_table, uint16_t first_cluster) {
        size_t count = 0;
        uint16_t cluster = first_cluster;
        while (!Is_End_Of_File(cluster)) {
            cluster = Get_Fat_Value(cluster, fat_table);
            count++;
        }

        return count;
    }

    inline uint16_t Get_File_Last_Cluster(const uint16_t *fat_table, uint16_t first_cluster) {
        if (first_cluster == 0) { // is root dir
            return first_cluster;
        }

        uint16_t last_cluster = first_cluster;

        uint16_t value;
        while (!Is_End_Of_File(value = Get_Fat_Value(last_cluster, fat_table))) {
            last_cluster = value;
        }

        return last_cluster;
    }

    void Split_To_Chains(const uint16_t *fat_table, uint16_t first_cluster, std::vector <Fat_Chain> &chains) {
        uint16_t current_cluster = first_cluster;
        uint16_t prev_cluster = first_cluster;
        uint16_t start = first_cluster;
        uint32_t size = 1;

        Fat_Chain chain = { 0 };
        while (!Is_End_Of_File(current_cluster)) {
            current_cluster = Get_Fat_Value(current_cluster, fat_table);

            if ((current_cluster - prev_cluster) == 1 && size < MAX_CHAIN_SIZE) {
                size++;
            } else {
                chain.start = start;
                chain.size = size;
                chains.push_back(chain);
                start = current_cluster;
                size = 1;
            }

            prev_cluster = current_cluster;
        }
    }

    kiv_os::NOS_Error Read_Cluster_Range(uint8_t disk_num, const Boot_Record &bpb, uint16_t cluster, uint32_t count,
                                         char *buffer, size_t buffer_size, size_t offset) {
        if (buffer_size == 0) {
            return kiv_os::NOS_Error::Success;
        }

        uint64_t start_sector = First_Data_Sector(bpb) + cluster * static_cast<uint64_t>(bpb.sectors_per_cluster);
        uint64_t sector_count = count * static_cast<uint64_t>(bpb.sectors_per_cluster);
        size_t sector_buffer_size = sector_count * bpb.bytes_per_sector;

        kiv_hal::NDisk_Status status;
        std::vector<char> sector_buffer;
        sector_buffer.resize(sector_buffer_size, 0);

        if (!kiv_krtl::Read_From_Disk(disk_num, start_sector, sector_count, sector_buffer.data(), status)) {
            return kiv_os::NOS_Error::IO_Error;
        }

        if (sector_buffer_size < buffer_size) {
            sector_buffer_size = buffer_size;
        } else if (buffer_size > 0 && (sector_buffer_size - offset) > buffer_size) {
            sector_buffer_size = buffer_size + offset;
        }

        std::memcpy(buffer, sector_buffer.data() + offset, sector_buffer_size - offset);
        return kiv_os::NOS_Error::Success;
    }

    kiv_os::NOS_Error Write_Cluster_Range(uint8_t disk_num, const Boot_Record &bpb, uint16_t cluster, uint32_t count,
                                          const char *buffer) {
        uint64_t start_sector = First_Data_Sector(bpb) + cluster * static_cast<uint64_t>(bpb.sectors_per_cluster);
        uint64_t sector_count = count * static_cast<uint64_t>(bpb.sectors_per_cluster);

        kiv_hal::NDisk_Status status;
        if (!kiv_krtl::Write_To_Disk(disk_num, start_sector, sector_count, buffer, status)) {
            return kiv_os::NOS_Error::IO_Error;
        }

        return kiv_os::NOS_Error::Success;
    }

    kiv_os::NOS_Error
    Allocate_Clusters(const Boot_Record &bpb, uint16_t *fat_table, uint16_t last_cluster, size_t count) {
        if (count == 0) {
            return kiv_os::NOS_Error::Success;
        }
        const size_t free_clusters = Get_Free_Clusters_Count(bpb, fat_table);
        if (free_clusters < count) {
            return kiv_os::NOS_Error::Not_Enough_Disk_Space;
        }

        uint16_t next_cluster;
        for (size_t i = 0; i < count; i++) {
            next_cluster = Get_Free_Cluster(bpb, fat_table);
            Set_Fat_Value(last_cluster, fat_table, next_cluster);
            Set_Fat_Value(next_cluster, fat_table, static_cast<uint16_t>(Fat_Entry_Value::EndOfFile));
            last_cluster = next_cluster;
        }

        return kiv_os::NOS_Error::Success;
    }

    uint16_t
    Get_Cluster_By_Offset(const Boot_Record &bpb, const uint16_t *fat_table, uint16_t first_cluster, uint64_t offset) {
        if (offset == 0) {
            return first_cluster;
        }

        size_t bytes_per_cluster = Bytes_Per_Cluster(bpb);
        size_t cluster_num = offset / bytes_per_cluster;
        uint16_t cluster = first_cluster;

        while (cluster_num > 0 && !Is_End_Of_File(cluster)) {
            cluster = Get_Fat_Value(cluster, fat_table);
            cluster_num--;
        }

        return cluster;
    }

    kiv_os::NOS_Error Load(uint8_t disk_num, kiv_hal::TDrive_Parameters &params,
                           Boot_Record &bpb, std::vector <uint16_t> &fat_table) {
        uint64_t count = (sizeof(Boot_Record) + params.bytes_per_sector - 1) / params.bytes_per_sector;
        fat_table.resize((count * params.bytes_per_sector) / sizeof(uint16_t), 0);
        kiv_hal::NDisk_Status status;

        // read boot record
        if (!kiv_krtl::Read_From_Disk(disk_num, 0, count, reinterpret_cast<char *>(fat_table.data()), status)) {
            return kiv_os::NOS_Error::IO_Error;
        }

        uint8_t *bpb_ptr = reinterpret_cast<uint8_t *>(fat_table.data()) + BOOT_SECTOR_OFFSET;
        bpb = *reinterpret_cast<Boot_Record *>(bpb_ptr);

        fat_table.resize((static_cast<int64_t>(bpb.sectors_per_fat) * bpb.bytes_per_sector) / sizeof(uint16_t), 0);
        // read fat table
        if (!kiv_krtl::Read_From_Disk(disk_num, bpb.reserved_sectors, bpb.sectors_per_fat,
                                      reinterpret_cast<char *>(fat_table.data()), status)) {
            return kiv_os::NOS_Error::IO_Error;
        }

        return kiv_os::NOS_Error::Success;
    }

    kiv_os::NOS_Error Update(uint8_t disk_num, const Boot_Record &bpb, const uint16_t *fat_table) {
        kiv_hal::NDisk_Status status;

        for (size_t i = 0; i < bpb.num_of_fats; i++) {
            uint64_t first_sector = bpb.reserved_sectors + i * bpb.sectors_per_fat;
            if (!kiv_krtl::Write_To_Disk(disk_num, first_sector, bpb.sectors_per_fat, (char *) fat_table, status)) {
                return kiv_os::NOS_Error::IO_Error;
            }
        }

        return kiv_os::NOS_Error::Success;
    }

    kiv_os::NOS_Error
    Read_File(uint8_t disk_num, const Boot_Record &bpb, const uint16_t *fat_table, const Dir_Entry &file,
              char *buffer, size_t buffer_size, size_t &read, uint64_t offset) {
        read = 0;
        if (buffer_size == 0) {
            return kiv_os::NOS_Error::Success;
        }

        size_t bytes_per_cluster = Bytes_Per_Cluster(bpb);
        uint16_t current_cluster = Get_Cluster_By_Offset(bpb, fat_table, file.first_cluster, offset);

        size_t pos = offset % bytes_per_cluster;
        size_t to_read;
        bool buffer_full = false;

        std::vector <Fat_Chain> chains;
        Split_To_Chains(fat_table, current_cluster, chains);

        for (size_t i = 0; i < chains.size(); i++) {
            to_read = chains[i].size * bytes_per_cluster - pos;

            if (read + to_read > buffer_size) {
                to_read = buffer_size - read;
                buffer_full = true;
            }
            if (!(file.attrs & static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory))) {
                if (read + to_read > file.size - offset) {
                    to_read = file.size - offset - read;
                    buffer_full = true;
                }
            }

            uint16_t cluster = chains[i].start;
            uint32_t count = chains[i].size;

            kiv_os::NOS_Error status = Read_Cluster_Range(disk_num, bpb, cluster, count, buffer + read, to_read, pos);
            if (status != kiv_os::NOS_Error::Success) {
                break;
            }

            read += to_read;
            pos = 0;

            if (buffer_full) {
                break;
            }
        }

        return kiv_os::NOS_Error::Success;
    }

    kiv_os::NOS_Error
    Read_Dir(uint8_t disk_num, const Boot_Record &bpb, const uint16_t *fat_table, const Dir_Entry &dir,
             std::vector <Dir_Entry> &items) {
        if (dir.first_cluster == 0) { // is root dir
            return Read_Root_Dir(disk_num, bpb, items);
        }

        const size_t cluster_count = Get_File_Clusters_Count(fat_table, dir.first_cluster);
        const size_t buffer_size = cluster_count * bpb.bytes_per_sector * bpb.sectors_per_cluster;
        const size_t dir_item_count = buffer_size / sizeof(Dir_Entry);

        size_t read = 0;
        std::vector<char> buffer;
        buffer.resize(buffer_size, 0);

        kiv_os::NOS_Error status = Read_File(disk_num, bpb, fat_table, dir, buffer.data(), buffer.size(), read, 0);
        if (status != kiv_os::NOS_Error::Success) {
            return status;
        }

        Dir_Entry *dir_items = reinterpret_cast<Dir_Entry *>(buffer.data());
        items.reserve(dir_item_count);

        for (size_t i = 0; i < dir_item_count; i++) {
            items.push_back(dir_items[i]);
        }

        return kiv_os::NOS_Error::Success;
    }

    kiv_os::NOS_Error Read_Root_Dir(uint8_t disk_num, const Boot_Record &bpb, std::vector <Dir_Entry> &items) {
        uint64_t first_sector = bpb.reserved_sectors + static_cast<uint64_t>(bpb.sectors_per_fat) * bpb.num_of_fats;
        uint64_t count = (bpb.max_root_entries * sizeof(Dir_Entry)) / bpb.bytes_per_sector;

        kiv_hal::NDisk_Status status;
        std::vector<char> buffer;
        buffer.resize(bpb.bytes_per_sector * count, 0);

        if (!kiv_krtl::Read_From_Disk(disk_num, first_sector, count, buffer.data(), status)) {
            return kiv_os::NOS_Error::IO_Error;
        }

        Dir_Entry *dir_items = reinterpret_cast<Dir_Entry *>(buffer.data());
        items.reserve(bpb.max_root_entries);

        for (size_t i = 0; i < bpb.max_root_entries; i++) {
            items.push_back(dir_items[i]);
        }

        return kiv_os::NOS_Error::Success;
    }

    kiv_os::NOS_Error Write_File(uint8_t disk_num, const Boot_Record &bpb, uint16_t *fat_table, Dir_Entry &file,
                                 const char *buffer, size_t buffer_size, size_t &written, uint64_t offset) {
        written = 0;
        if (file.attrs & static_cast<uint16_t>(kiv_os::NFile_Attributes::Directory)) {
            return kiv_os::NOS_Error::Invalid_Argument;
        }
        if (buffer_size == 0) {
            return kiv_os::NOS_Error::Success;
        }

        const uint16_t last_cluster = Get_File_Last_Cluster(fat_table, file.first_cluster);
        const size_t count = Get_File_Clusters_Count(fat_table, file.first_cluster);
        const size_t bytes_per_cluster = Bytes_Per_Cluster(bpb);
        const size_t allocated = count * bytes_per_cluster;
        size_t fill_bytes = 0;
        size_t new_bytes = 0;
        size_t to_write = 0;

        std::vector<char> cluster_buffer;
        cluster_buffer.resize(bytes_per_cluster, 0);

        if ((offset + buffer_size) > allocated) {
            if (offset > allocated) {
                fill_bytes = static_cast<size_t>(offset) - allocated;
                new_bytes = fill_bytes + buffer_size;
            } else {
                new_bytes = buffer_size - (allocated - static_cast<size_t>(offset));
            }
        }

        size_t new_clusters = (new_bytes + bytes_per_cluster - 1) / bytes_per_cluster;
        kiv_os::NOS_Error status = Allocate_Clusters(bpb, fat_table, last_cluster, new_clusters);
        if (status != kiv_os::NOS_Error::Success) {
            return status;
        }

        uint16_t current_cluster;
        if (fill_bytes > 0) {
            current_cluster = Get_Cluster_By_Offset(bpb, fat_table, file.first_cluster, allocated);
        } else {
            current_cluster = Get_Cluster_By_Offset(bpb, fat_table, file.first_cluster, static_cast<size_t>(offset));
        }

        size_t written_bytes = 0;
        while (written_bytes < fill_bytes) {
            status = Write_Cluster_Range(disk_num, bpb, current_cluster, 1, cluster_buffer.data());
            if (status != kiv_os::NOS_Error::Success) {
                return status;
            }
            written_bytes += bytes_per_cluster;
            current_cluster = Get_Fat_Value(current_cluster, fat_table);
        }

        written += written_bytes;
        written_bytes = 0;

        size_t cluster_offset = offset % bytes_per_cluster;
        to_write = bytes_per_cluster - cluster_offset;
        if (to_write > buffer_size) {
            to_write = buffer_size;
        }

        status = Read_Cluster_Range(disk_num, bpb, current_cluster, 1, cluster_buffer.data(), bytes_per_cluster, 0);
        if (status != kiv_os::NOS_Error::Success) {
            return status;
        }

        std::memcpy(cluster_buffer.data() + cluster_offset, buffer, to_write);

        status = Write_Cluster_Range(disk_num, bpb, current_cluster, 1, cluster_buffer.data());
        if (status != kiv_os::NOS_Error::Success) {
            return status;
        }

        written_bytes += to_write;
        if ((written_bytes + bytes_per_cluster) <= buffer_size) {
            to_write = bytes_per_cluster;

            while (written_bytes <= (buffer_size - bytes_per_cluster)) {
                current_cluster = Get_Fat_Value(current_cluster, fat_table);
                status = Write_Cluster_Range(disk_num, bpb, current_cluster, 1, buffer + written_bytes);
                if (status != kiv_os::NOS_Error::Success) {
                    return status;
                }

                written_bytes += to_write;
            }
        }

        current_cluster = Get_Fat_Value(current_cluster, fat_table);

        if (written_bytes != buffer_size) {
            status = Read_Cluster_Range(disk_num, bpb, current_cluster, 1, cluster_buffer.data(), bytes_per_cluster, 0);
            if (status != kiv_os::NOS_Error::Success) {
                return status;
            }

            to_write = buffer_size - written_bytes;
            std::memcpy(cluster_buffer.data(), buffer + written_bytes, to_write);

            status = Write_Cluster_Range(disk_num, bpb, current_cluster, 1, cluster_buffer.data());
            if (status != kiv_os::NOS_Error::Success) {
                return status;
            }
        }

        if ((offset + buffer_size) > file.size) {
            file.size = static_cast<uint32_t>(offset + buffer_size);
        }
        written += written_bytes;

        return Update(disk_num, bpb, fat_table);
    }

    kiv_os::NOS_Error Create_File(uint8_t disk_num, const Boot_Record &bpb, uint16_t *fat_table,
                                  const Dir_Entry &parent_dir, Dir_Entry &new_file) {
        const size_t bytes_per_cluster = Bytes_Per_Cluster(bpb);
        const uint16_t last_dir_cluster = Get_File_Last_Cluster(fat_table, parent_dir.first_cluster);
        uint16_t next_free_cluster = Get_Free_Cluster(bpb, fat_table);
        if (next_free_cluster == 0) {
            return kiv_os::NOS_Error::Not_Enough_Disk_Space;
        }

        new_file.first_cluster = next_free_cluster;
        new_file.size = 0;
        Set_Fat_Value(new_file.first_cluster, fat_table, static_cast<uint16_t>(Fat_Entry_Value::EndOfFile));

        std::vector<char> buffer;
        buffer.resize(bytes_per_cluster, 0);

        kiv_os::NOS_Error status = Update_File(disk_num, bpb, fat_table, parent_dir, new_file, true, false);
        if (status == kiv_os::NOS_Error::File_Not_Found) {
            if (parent_dir.first_cluster == 0) { // is root dir
                return kiv_os::NOS_Error::Not_Enough_Disk_Space;
            }

            next_free_cluster = Get_Free_Cluster(bpb, fat_table);
            if (next_free_cluster == 0) {
                return kiv_os::NOS_Error::Not_Enough_Disk_Space;
            }

            Set_Fat_Value(last_dir_cluster, fat_table, next_free_cluster);
            Set_Fat_Value(next_free_cluster, fat_table, static_cast<uint16_t>(Fat_Entry_Value::EndOfFile));

            status = Write_Cluster_Range(disk_num, bpb, next_free_cluster, 1, buffer.data());
            if (status != kiv_os::NOS_Error::Success) {
                return status;
            }

            status = Update_File(disk_num, bpb, fat_table, parent_dir, new_file, true, false);
            if (status != kiv_os::NOS_Error::Success) {
                return status;
            }
        } else if (status != kiv_os::NOS_Error::Success) {
            return status;
        }

        if (new_file.attrs & static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory)) {
            // save self and parent item
            kiv_fat::Dir_Entry self = { 0 };
            self.attrs = new_file.attrs;
            self.size = new_file.size;
            self.write_date = new_file.write_date;
            self.write_time = new_file.write_time;
            self.first_cluster = new_file.first_cluster;
            std::memset(self.name, 0x20, FILE_NAME_LENGTH);
            self.name[0] = '.';

            kiv_fat::Dir_Entry parent = { 0 };
            parent.attrs = parent_dir.attrs;
            parent.size = parent_dir.size;
            parent.write_date = parent_dir.write_date;
            parent.write_time = parent_dir.write_time;
            parent.first_cluster = parent_dir.first_cluster;
            std::memset(parent.name, 0x20, FILE_NAME_LENGTH);
            parent.name[0] = '.';
            parent.name[1] = '.';

            std::memcpy(buffer.data(), &self, sizeof(Dir_Entry));
            std::memcpy(buffer.data() + sizeof(Dir_Entry), &parent, sizeof(Dir_Entry));
        }

        status = Write_Cluster_Range(disk_num, bpb, new_file.first_cluster, 1, buffer.data());
        if (status != kiv_os::NOS_Error::Success) {
            return status;
        }

        return Update(disk_num, bpb, fat_table);
    }

    kiv_os::NOS_Error Delete_File(uint8_t disk_num, const Boot_Record &bpb, uint16_t *fat_table,
                                  const Dir_Entry &parent_dir, Dir_Entry &file) {
        uint16_t cluster = file.first_cluster;
        kiv_os::NOS_Error status = Update_File(disk_num, bpb, fat_table, parent_dir, file, false, true);
        if (status != kiv_os::NOS_Error::Success) {
            return status;
        }

        uint16_t prev_cluster;
        while (!Is_End_Of_File(cluster)) {
            prev_cluster = cluster;
            cluster = Get_Fat_Value(cluster, fat_table);
            Set_Fat_Value(prev_cluster, fat_table, static_cast<uint16_t>(Fat_Entry_Value::Unused));
        }

        return Update(disk_num, bpb, fat_table);
    }

    kiv_os::NOS_Error Update_File(uint8_t disk_num, const Boot_Record &bpb, const uint16_t *fat_table,
                                  const Dir_Entry &parent_dir, Dir_Entry &file, bool create, bool remove) {
        const size_t bytes_per_cluster = Bytes_Per_Cluster(bpb);
        size_t cluster_count;
        size_t max_items_dir;

        if (parent_dir.first_cluster == 0) { // is root dir
            cluster_count = (bpb.max_root_entries * sizeof(Dir_Entry)) / bpb.bytes_per_sector;
            max_items_dir = bpb.max_root_entries;
        } else {
            cluster_count = Get_File_Clusters_Count(fat_table, parent_dir.first_cluster);
            max_items_dir = (cluster_count * bytes_per_cluster) / sizeof(Dir_Entry);
        }

        std::vector<char> buffer;
        buffer.resize(bytes_per_cluster * cluster_count, 0);

        size_t read = 0;
        kiv_os::NOS_Error status = kiv_os::NOS_Error::Success;
        if (parent_dir.first_cluster == 0) { // is root dir
            kiv_hal::NDisk_Status s;
            uint64_t first_sector = bpb.reserved_sectors + static_cast<uint64_t>(bpb.sectors_per_fat) * bpb.num_of_fats;
            if (!kiv_krtl::Read_From_Disk(disk_num, first_sector, cluster_count, buffer.data(), s)) {
                status = kiv_os::NOS_Error::IO_Error;
            }
        } else {
            status = Read_File(disk_num, bpb, fat_table, parent_dir, buffer.data(),
                               buffer.size(), read, 0);
        }
        if (status != kiv_os::NOS_Error::Success) {
            return status;
        }

        Dir_Entry *items = reinterpret_cast<Dir_Entry *>(buffer.data());
        size_t pos = 0;
        size_t empty_pos = -1;
        for (; pos < max_items_dir; pos++) {
            if (std::strcmp(file.name, items[pos].name) == 0) {
                break;
            }
            if (items[pos].name[0] == 0 && empty_pos == -1) {
                empty_pos = pos;
            }
        }

        if (pos >= max_items_dir) {
            if (create && empty_pos != -1) {
                pos = empty_pos;
            } else {
                return kiv_os::NOS_Error::File_Not_Found;
            }
        }

        if (remove) {
            Clear_Dir_Entry(file);
        }

        size_t offset = pos * sizeof(Dir_Entry);
        std::memcpy(buffer.data() + offset, &file, sizeof(Dir_Entry));

        if (parent_dir.first_cluster == 0) { // is root dir
            uint64_t first_sector = bpb.reserved_sectors + static_cast<uint64_t>(bpb.sectors_per_fat) * bpb.num_of_fats;
            kiv_hal::NDisk_Status s;
            if (!kiv_krtl::Write_To_Disk(disk_num, first_sector, cluster_count, buffer.data(), s)) {
                return kiv_os::NOS_Error::IO_Error;
            } else {
                return kiv_os::NOS_Error::Success;
            }
        }

        uint16_t cluster1 = static_cast<uint16_t>(offset / bytes_per_cluster);
        uint16_t cluster2 = static_cast<uint16_t>((offset + sizeof(Dir_Entry) - 1) / bytes_per_cluster);

        uint16_t cluster = parent_dir.first_cluster;
        uint16_t c;
        for (uint16_t i = 0; i < cluster1 && !Is_End_Of_File(c = Get_Fat_Value(cluster, fat_table)); i++) {
            cluster = c;
        }

        status = Write_Cluster_Range(disk_num, bpb, cluster, 1, buffer.data() + (cluster1 * bytes_per_cluster));
        if (status != kiv_os::NOS_Error::Success) {
            return status;
        }

        if (cluster1 != cluster2) {
            cluster = parent_dir.first_cluster;
            for (uint16_t i = 0; i < cluster2 && !Is_End_Of_File(c = Get_Fat_Value(cluster, fat_table)); i++) {
                cluster = c;
            }

            status = Write_Cluster_Range(disk_num, bpb, cluster, 1, buffer.data() + (cluster2 * bytes_per_cluster));
            if (status != kiv_os::NOS_Error::Success) {
                return status;
            }
        }

        return kiv_os::NOS_Error::Success;
    }

    kiv_os::NOS_Error Resize_File(uint8_t disk_num, const Boot_Record &bpb, uint16_t *fat_table,
                                  const Dir_Entry &parent_dir, Dir_Entry &file, uint32_t new_size) {
        const size_t old_size = file.size;

        if (old_size == new_size) {
            return kiv_os::NOS_Error::Success;
        }

        const size_t bytes_per_cluster = Bytes_Per_Cluster(bpb);
        const size_t old_count = (old_size + bytes_per_cluster - 1) / bytes_per_cluster;
        size_t new_count = (new_size + bytes_per_cluster - 1) / bytes_per_cluster;
        if (new_count == 0) {
            new_count = 1;
        }
        file.size = new_size;

        bool Update_fat = false;
        kiv_os::NOS_Error status;

        if (new_size > old_size) {
            std::vector<char> buffer;
            buffer.resize(new_size - old_size, 0);

            size_t written;
            status = Write_File(disk_num, bpb, fat_table, file, buffer.data(), buffer.size(), written, old_size);
            if (status != kiv_os::NOS_Error::Success) {
                return status;
            }
        } else if (new_count < old_count) {
            Update_fat = true;

            uint16_t cluster = file.first_cluster;
            size_t i = 1;
            uint16_t new_end = cluster;

            while (!Is_End_Of_File(cluster)) {
                uint16_t tmp = cluster;
                cluster = Get_Fat_Value(cluster, fat_table);

                if (i > new_count) {
                    Set_Fat_Value(tmp, fat_table, static_cast<uint16_t>(Fat_Entry_Value::Unused));
                }

                if (i == new_count) {
                    new_end = tmp;
                }

                i++;
            }

            Set_Fat_Value(new_end, fat_table, static_cast<uint16_t>(Fat_Entry_Value::EndOfFile));
        }

        status = Update_File(disk_num, bpb, fat_table, parent_dir, file, false, false);
        if (status != kiv_os::NOS_Error::Success) {
            return status;
        }

        if (Update_fat) {
            status = Update(disk_num, bpb, fat_table);
            if (status != kiv_os::NOS_Error::Success) {
                return status;
            }
        }

        return kiv_os::NOS_Error::Success;
    }

    kiv_os::NOS_Error Find_File(uint8_t disk_num, const Boot_Record &bpb, const uint16_t *fat_table,
                                fs::path &path, Dir_Entry &file, Dir_Entry &parent_dir, bool *last) {
        parent_dir.first_cluster = 0;
        parent_dir.attrs = static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory);
        if (path.relative_path().empty()) {
            file.first_cluster = 0; // mark as root dir
            file.attrs = static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory);
            return kiv_os::NOS_Error::Success;
        }

        std::vector <Dir_Entry> items;
        kiv_os::NOS_Error status = Read_Root_Dir(disk_num, bpb, items);
        if (status != kiv_os::NOS_Error::Success) {
            return status;
        }

        std::string s_path = path.relative_path().string();
        std::replace(s_path.begin(), s_path.end(), '/', '\\');
        if (s_path.back() == '\\') {
            s_path.resize(s_path.size() - 1);
        }
        size_t parts = std::count(s_path.begin(), s_path.end(), '\\');
        path = s_path;
        std::string file_name = Get_File_Name(path.filename());

        int i = 0;
        for (auto &part : path) {
            std::string part_str = Get_File_Name(part);

            Dir_Entry *item = nullptr;
            for (Dir_Entry &i_item : items) {
                if (part_str.compare(0, FILE_NAME_LENGTH, i_item.name, FILE_NAME_LENGTH) == 0) {
                    item = &i_item;
                    break;
                }
            }

            bool is_last = i == parts && (part_str.compare(file_name) == 0);
            if (last != nullptr) {
                *last = is_last;
            }
            if (item == nullptr) {
                return kiv_os::NOS_Error::File_Not_Found;
            }

            if (is_last) {
                file = *item;
                break;
            }

            if (!(item->attrs & static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory))) {
                return kiv_os::NOS_Error::File_Not_Found;
            }

            parent_dir = *item;
            items.clear();

            status = Read_Dir(disk_num, bpb, fat_table, parent_dir, items);
            if (status != kiv_os::NOS_Error::Success) {
                return status;
            }
            i++;
        }

        return kiv_os::NOS_Error::Success;
    }

}
