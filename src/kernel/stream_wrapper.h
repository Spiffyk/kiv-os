#pragma once

#include <memory>
#include "Stream.h"
#include "stream_pipe.h"

class Wrapper_Stream : public Stream {

	const bool read_allowed;
	const bool write_allowed;
	std::shared_ptr<Stream> backend;

public:

	/**
	* Creates a new stream wrapping the specified stream.
	* 
	* @param to_wrap - the stream to be wrapped in this wrapper
	* @param allow_read - whether the wrapper should allow reading of the wrapped stream
	* @param allow_write - whether the wrapper should allow writing into the wrapped stream
	*/
	Wrapper_Stream(std::shared_ptr<Stream>&& to_wrap, const bool allow_read, const bool allow_write);

	kiv_os::NOS_Error Write(const void* buffer, const size_t size, size_t& written) override;

	kiv_os::NOS_Error Read(void* buffer, const size_t size, size_t& read) override;

	kiv_os::NOS_Error Seek(const int64_t offset, const kiv_os::NFile_Seek whence) override;

	kiv_os::NOS_Error Resize(const int64_t offset, const kiv_os::NFile_Seek whence) override;

	int64_t Get_Seek() override;

	bool Is_Writable() override;

	bool Is_Readable() override;

	bool Is_Seekable() override;

	bool Is_Resizable() override;

	void Terminate() override;

	bool Is_Terminated() override;

	virtual ~Wrapper_Stream();
};
