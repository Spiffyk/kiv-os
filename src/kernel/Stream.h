#pragma once

#include <cstdint>
#include "../api/api.h"

/**
* An abstract interface for a data stream. Used for reading/writing files and other stream-like structures.
*/
class Stream {
public:
	/**
	* Writes data from the given buffer into the stream
	* 
	* @param buffer - buffer from which data is written
	* @param size - size of data to write
	* @param written - size of written data
	* @return success true/false
	*/
	virtual kiv_os::NOS_Error Write(const void* buffer, const size_t size, size_t& written) = 0;

	/**
	* Reads data from the stream into the given buffer.
	* 
	* @param buffer - buffer to which data is read
	* @param size - size of data to read
	* @param read - size of read data; if zero, the stream is at its end
	* @return success true/false
	*/
	virtual kiv_os::NOS_Error Read(void* buffer, const size_t size, size_t& read) = 0;

	/**
	* Seeks position in stream. Position is relative to NFile_Seek which can be Beginning, Current, End.
	* Position is then given with the offset from that relative location.
	* 
	* @param offset - offset from relative location
	* @param whence - from where to seek (relative location)
	* @return success true/false
	*/
	virtual kiv_os::NOS_Error Seek(const int64_t offset, const kiv_os::NFile_Seek whence) = 0;

	/**
	* Seeks position in stream and resize file to that position.
	* Position is relative to NFile_Seek, which can be Beginning, Current, End.
	* Position is then given with the offset from that NFile_Seek location.
	* @param offset - offset from relative location
	* @param whence - from where to seek (relative location)
	* @return success true/false
	*/
	virtual kiv_os::NOS_Error Resize(const int64_t offset, const kiv_os::NFile_Seek whence) = 0;

	/**
	* Gets current position in stream
	*/
	virtual int64_t Get_Seek() = 0;

	/**
	* If Stream is writable
	*/
	virtual bool Is_Writable() = 0;

	/**
	* If Stream is readable
	*/
	virtual bool Is_Readable() = 0;

	/**
	* If Stream is seekable
	*/
	virtual bool Is_Seekable() = 0;

	/**
	* If Stream is resizable
	*/
	virtual bool Is_Resizable() = 0;

	/**
	* Terminates the stream so that it does not produce nor consume any more bytes.
	*/
	virtual void Terminate() = 0;

	/**
	* Checks whether the stream has been terminated.
	*/
	virtual bool Is_Terminated() = 0;

	/**
	* A destructor that correctly closes the stream
	*/
	virtual ~Stream() {};
};
