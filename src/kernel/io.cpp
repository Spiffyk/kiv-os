#include <map>
#include <mutex>
#include <memory>

#include "io.h"
#include "kernel.h"
#include "handles.h"
#include "stream.h"
#include "stream_pipe.h"
#include "stream_wrapper.h"
#include "stream_console.h"
#include "process.h"


std::mutex Stream_Table_Mutex;
std::map<kiv_os::THandle, std::shared_ptr<Stream>> Stream_Table;


kiv_os::THandle Console_In = kiv_os::Invalid_Handle;
kiv_os::THandle Console_Out = kiv_os::Invalid_Handle;


void Make_Path_Absolute(fs::path &path) {
    if (!path.is_absolute() && !path.has_root_path()) {
		fs::path current = Current_Process_Working_Dir();
        path = current / path;
    }
	path = (path.root_path() / path.relative_path()).lexically_normal();
}

void Handle_Open_File(kiv_hal::TRegisters &regs) {
	std::lock_guard<std::mutex> lck_st(Stream_Table_Mutex);

    const char *path_str = reinterpret_cast<const char *>(regs.rdx.r);
    kiv_os::NOpen_File flags = static_cast<kiv_os::NOpen_File>(regs.rcx.l);
    kiv_os::NFile_Attributes attrs = static_cast<kiv_os::NFile_Attributes>(regs.rdi.i);

    fs::path path(path_str);
    Make_Path_Absolute(path);

    std::unique_ptr <Stream> stream;
    auto error = Get_File_System()->Open(path, stream, flags, attrs);
    if (error != kiv_os::NOS_Error::Success) {
        Debug_Log("Cannot open file");
        regs.rax.x = static_cast<uint16_t>(kiv_os::Invalid_Handle);
        return;
    }

    auto file_handle = Alloc_Handle();
    regs.rax.x = static_cast<uint16_t>(file_handle);
    if (file_handle == kiv_os::Invalid_Handle) {
        Set_Error(regs, kiv_os::NOS_Error::Out_Of_Memory);
        Debug_Log("Could not allocate another handle");
        return;
    }

	Stream_Table.emplace(file_handle, std::move(stream));
    Current_Process_Own_Handle(file_handle);
}

void Handle_Write_File(kiv_hal::TRegisters& regs) {
	std::shared_ptr<Stream> stream;
	{
		std::lock_guard<std::mutex> lck_st(Stream_Table_Mutex);
		auto& it = Stream_Table.find(regs.rdx.x);
		if (it == Stream_Table.end()) {
			Debug_Log("Attempt to write into a non-existent stream");
			Set_Error(regs, kiv_os::NOS_Error::File_Not_Found);
			return;
		}

		stream = it->second;
	}

	if (!stream->Is_Writable()) {
		Debug_Log("Attempted to write into a non-writable stream");
		Set_Error(regs, kiv_os::NOS_Error::IO_Error);
		return;
	}

	const void* buffer = reinterpret_cast<void*>(regs.rdi.r);
	const size_t size = regs.rcx.r;
	size_t written = 0;
	auto error = stream->Write(buffer, size, written);

	if (error != kiv_os::NOS_Error::Success) {
		Debug_Log("Stream reported an error while writing");
		Set_Error(regs, error);
		return;
	}

	regs.rax.r = written;
}

void Handle_Read_File(kiv_hal::TRegisters& regs) {
	std::shared_ptr<Stream> stream;
	{
		std::lock_guard<std::mutex> lck_st(Stream_Table_Mutex);
		auto& it = Stream_Table.find(regs.rdx.x);
		if (it == Stream_Table.end()) {
			Debug_Log("Attempt to read from a non-existent stream");
			Set_Error(regs, kiv_os::NOS_Error::File_Not_Found);
			return;
		}

		stream = it->second;
	}

	if (!stream->Is_Readable()) {
		Debug_Log("Attempted to read from a non-readable stream");
		Set_Error(regs, kiv_os::NOS_Error::IO_Error);
		return;
	}

	void* buffer = reinterpret_cast<void*>(regs.rdi.r);
	const size_t size = regs.rcx.r;
	size_t read = 0;
	auto error = stream->Read(buffer, size, read);

	if (error != kiv_os::NOS_Error::Success) {
		Debug_Log("Stream reported an error while reading");
		Set_Error(regs, error);
		return;
	}

	regs.rax.r = read;
}

void Handle_Seek(kiv_hal::TRegisters& regs) {
	std::shared_ptr<Stream> stream;
	{
		std::lock_guard<std::mutex> lck_st(Stream_Table_Mutex);
		auto& it = Stream_Table.find(regs.rdx.x);
		if (it == Stream_Table.end()) {
			Debug_Log("Attempt to seek a non-existent stream");
			Set_Error(regs, kiv_os::NOS_Error::File_Not_Found);
			return;
		}

		stream = it->second;
	}

	if (!stream->Is_Seekable()) {
		Debug_Log("Attempted to seek a non-seekable stream");
		Set_Error(regs, kiv_os::NOS_Error::IO_Error);
		return;
	}

	int64_t offset = regs.rdi.r;
	kiv_os::NFile_Seek whence = static_cast<kiv_os::NFile_Seek>(regs.rcx.l);
	auto error = kiv_os::NOS_Error::Success;

	switch (static_cast<kiv_os::NFile_Seek>(regs.rcx.h)) {
	case kiv_os::NFile_Seek::Set_Position:
		error = stream->Seek(offset, whence);
		break;
	case kiv_os::NFile_Seek::Set_Size:
		if (!stream->Is_Resizable()) {
			Debug_Log("Attempted to resize a non-resizable stream");
			Set_Error(regs, kiv_os::NOS_Error::IO_Error);
			return;
		}
		error = stream->Resize(offset, whence);
		break;
	case kiv_os::NFile_Seek::Get_Position:
		regs.rax.r = stream->Get_Seek();
		break;
	}

	if (error != kiv_os::NOS_Error::Success) {
		Debug_Log("Stream reported an error while processing a seek operation");
		Set_Error(regs, error);
	}
}

void Terminate_Handle(kiv_os::THandle handle) {
	std::lock_guard<std::mutex> lck_st(Stream_Table_Mutex);

	auto& it = Stream_Table.find(handle);
	if (it == Stream_Table.end()) {
		Debug_Log("(WARN) Attempted to terminate a non-existent stream.");
		return;
	}

	it->second->Terminate();
}

void Close_Handle(kiv_os::THandle handle) {
	std::lock_guard<std::mutex> lck_st(Stream_Table_Mutex);

	auto result = Stream_Table.erase(handle);

	if (result) {
		Remove_Handle(handle);
	} else {
		Debug_Log("(WARN) Attempted to close a non-existent stream.");
	}
}

void Handle_Close_Handle(kiv_hal::TRegisters& regs) {
	kiv_os::THandle handle = regs.rdx.x;
	if (!Check_Current_Process_Owns_Handle(handle)) {
		Debug_Log("Process attempted to close a handle it does not own");
		Set_Error(regs, kiv_os::NOS_Error::Permission_Denied);
		return;
	}

	Current_Process_Disown_Handle(handle);
	Close_Handle(handle);
}

void Handle_Delete_File(kiv_hal::TRegisters &regs) {
    const char *path_str = reinterpret_cast<const char *>(regs.rdx.r);
    fs::path path(path_str);
    Make_Path_Absolute(path);

    auto error = Get_File_System()->Remove(path);
    if (error != kiv_os::NOS_Error::Success) {
        Debug_Log("Cannot delete file");
        return;
    }
}

void Handle_Set_Working_Dir(kiv_hal::TRegisters& regs) {
	const char* path_str = reinterpret_cast<const char*>(regs.rdx.r);
	fs::path path(path_str);
	Make_Path_Absolute(path);

	kiv_os::NFile_Attributes attributes;
	Get_File_System()->Get_File_Attributes(path, attributes);
	if (!(static_cast<uint8_t>(attributes) & static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory))) {
		Debug_Log("`" << path << "` is not a directory");
		Set_Error(regs, kiv_os::NOS_Error::Invalid_Argument);
		return;
	}

	Set_Current_Process_Working_Dir(path);
}

void Handle_Get_Working_Dir(kiv_hal::TRegisters& regs) {
	char* buffer = reinterpret_cast<char*>(regs.rdx.r);
	size_t size = static_cast<size_t>(regs.rcx.r);

	fs::path working_dir = Current_Process_Working_Dir();
	std::string working_dir_s = working_dir.string();
	strncpy_s(buffer, size, working_dir_s.c_str(), working_dir_s.size());
	regs.rax.r = strnlen(buffer, size - 1) + 1;
}

void Handle_Set_File_Attribute(kiv_hal::TRegisters &regs) {
    const char *path_str = reinterpret_cast<const char *>(regs.rdx.r);
    fs::path path(path_str);
    Make_Path_Absolute(path);

    kiv_os::NFile_Attributes attributes = static_cast<kiv_os::NFile_Attributes>(regs.rdi.i);
    auto error = Get_File_System()->Set_File_Attributes(path, attributes);
    if (error != kiv_os::NOS_Error::Success) {
        Debug_Log("Cannot set file attributes");
        return;
    }
}

void Handle_Get_File_Attribute(kiv_hal::TRegisters &regs) {
    const char *path_str = reinterpret_cast<const char *>(regs.rdx.r);
    fs::path path(path_str);
    Make_Path_Absolute(path);

    kiv_os::NFile_Attributes attributes;
    auto error = Get_File_System()->Get_File_Attributes(path, attributes);
    if (error != kiv_os::NOS_Error::Success) {
        Debug_Log("Cannot read file attributes");
        return;
    }

    regs.rdi.i = static_cast<uint8_t>(attributes);
}

void Handle_Create_Pipe(kiv_hal::TRegisters& regs) {
	std::shared_ptr<Pipe> pipe = std::make_shared<Pipe>();

	{
		std::lock_guard<std::mutex> lck_st(Stream_Table_Mutex);

		kiv_os::THandle write_hnd = Alloc_Handle();
		Stream_Table.emplace(write_hnd, std::make_unique<Wrapper_Stream>(pipe, false, true));
		Current_Process_Own_Handle(write_hnd);

		kiv_os::THandle read_hnd = Alloc_Handle();
		Stream_Table.emplace(read_hnd, std::make_unique<Wrapper_Stream>(pipe, true, false));
		Current_Process_Own_Handle(read_hnd);

		kiv_os::THandle* out = reinterpret_cast<decltype(out)>(regs.rdx.r);
		out[0] = write_hnd;
		out[1] = read_hnd;
	}
}

void Handle_IO(kiv_hal::TRegisters& regs) {

	switch (static_cast<kiv_os::NOS_File_System>(regs.rax.l)) {
	case kiv_os::NOS_File_System::Open_File:
		Handle_Open_File(regs);
		break;
	case kiv_os::NOS_File_System::Write_File:
		Handle_Write_File(regs);
		break;
	case kiv_os::NOS_File_System::Read_File:
		Handle_Read_File(regs);
		break;
	case kiv_os::NOS_File_System::Seek:
		Handle_Seek(regs);
		break;
	case kiv_os::NOS_File_System::Close_Handle:
		Handle_Close_Handle(regs);
		break;
	case kiv_os::NOS_File_System::Delete_File:
		Handle_Delete_File(regs);
		break;
	case kiv_os::NOS_File_System::Set_Working_Dir:
		Handle_Set_Working_Dir(regs);
		break;
	case kiv_os::NOS_File_System::Get_Working_Dir:
		Handle_Get_Working_Dir(regs);
		break;
	case kiv_os::NOS_File_System::Set_File_Attribute:
		Handle_Set_File_Attribute(regs);
		break;
	case kiv_os::NOS_File_System::Get_File_Attribute:
		Handle_Get_File_Attribute(regs);
		break;
	case kiv_os::NOS_File_System::Create_Pipe:
		Handle_Create_Pipe(regs);
		break;
	default:
		Set_Error(regs, kiv_os::NOS_Error::Invalid_Argument);
	}
}


void Initialize_IO() {
	std::lock_guard<std::mutex> lck_st(Stream_Table_Mutex);

	kiv_os::THandle cin_hnd = Alloc_Handle();
	Stream_Table.emplace(cin_hnd, std::make_unique<Console_In_Stream>());

	kiv_os::THandle cout_hnd = Alloc_Handle();
	Stream_Table.emplace(cout_hnd, std::make_unique<Console_Out_Stream>());

	Console_In = cin_hnd;
	Console_Out = cout_hnd;
}

void Terminate_IO() {
	std::lock_guard<std::mutex> lck_st(Stream_Table_Mutex);
	std::for_each(Stream_Table.begin(), Stream_Table.end(), [](auto& p) {
		p.second->Terminate();
	});
}

kiv_os::THandle Get_Console_In() {
	return Console_In;
}

kiv_os::THandle Get_Console_Out() {
	return Console_Out;
}
