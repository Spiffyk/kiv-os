#include <map>
#include <list>
#include <set>
#include <thread>
#include <mutex>
#include <algorithm>
#include <filesystem>
#include <string>

#include "../api/api.h"
#include "kernel.h"
#include "handles.h"
#include "io.h"
#include "krtl.h"
#include "stream_inmemory.h"
#include "util.h"

#include "process.h"

const size_t CMD_BUFFER_LENGTH = 256;
const char* PROC_FS_ENTRY_NAME = "NAME    PRI";
const std::string PROC_FS_ENTRY_NAME_S = "name.pri";
const char* PROC_FS_ENTRY_THRCOUNT = "THRCOUNTPRI";
const std::string PROC_FS_ENTRY_THRCOUNT_S = "thrcount.pri";

/*
 * Available process states.
 */
enum class ThreadState : uint8_t {
	READY = 1,
	RUNNING,
	ZOMBIE
};

struct Process;
struct ThreadHolder;
void Process_Thread_Procedure(ThreadHolder& holder, Process& process, kiv_os::TThread_Proc procedure, kiv_hal::TRegisters regs, bool main_thread);

struct ThreadHolder {
	std::thread thread;
	kiv_os::THandle handle;
	ThreadState state = ThreadState::READY;

	ThreadHolder(kiv_os::THandle hnd, Process& process, kiv_os::TThread_Proc procedure, kiv_hal::TRegisters& regs, bool main_thread) noexcept {
		handle = hnd;
		thread = std::thread(Process_Thread_Procedure, std::ref(*this), std::ref(process), procedure, regs, main_thread);
		Assign_Thread_Handle(hnd, thread.get_id());
		thread.detach();
	}
};

/*
 * Process metadata structure.
 */
struct Process {
	std::recursive_mutex mutex;

	char command[CMD_BUFFER_LENGTH] = { 0 };

	// process handle
	kiv_os::THandle handle;

	// process state (should be the common denominator of thread states)
	ThreadState state = ThreadState::READY;

	uint16_t exit_code = 0; // default exit code is 0, to change, use NOS_Process::Exit

	// process thread holders
	std::map<kiv_os::THandle, ThreadHolder> threads;

	// number of threads in the process that are currently marked as running
	size_t running_threads = 0;

	// handle of the parent process (if any exists)
	kiv_os::THandle parent = kiv_os::Invalid_Handle;

	// handles of child processes
	std::set<kiv_os::THandle> children;

	// handles owned by this process - will be auto-closed once the process exits
	std::set<kiv_os::THandle> owned_handles;

	// map of signal handler functions
	std::map<kiv_os::NSignal_Id, kiv_os::TThread_Proc> signal_handlers;

	fs::path working_dir;

	Process(kiv_os::THandle h) : handle(h) {}
};

std::recursive_mutex Process_Table_Mutex;
std::map<kiv_os::THandle, Process> Process_Table;

std::recursive_mutex Thread_To_Process_Mutex;
std::map<kiv_os::THandle, kiv_os::THandle> Thread_To_Process;



kiv_os::THandle Current_Thread_Handle() {
	auto thread_id = std::this_thread::get_id();
	return To_kiv_os_Handle(thread_id);
}

kiv_os::THandle Current_Process_Handle() {
	std::lock_guard<std::recursive_mutex> ttp_lck(Thread_To_Process_Mutex);

	auto thread_handle = Current_Thread_Handle();
	if (thread_handle == kiv_os::Invalid_Handle) {
		// this will happen if the "current process" is the kernel - this is a valid state!
		return kiv_os::Invalid_Handle;
	}

	auto it = Thread_To_Process.find(thread_handle);
	return (it == Thread_To_Process.end())
		? kiv_os::Invalid_Handle
		: it->second;
}



void Process_Thread_Procedure(ThreadHolder& holder, Process& process, kiv_os::TThread_Proc procedure, kiv_hal::TRegisters regs, bool main_thread) {
	auto handle = Current_Thread_Handle();

	{
		std::lock_guard<std::recursive_mutex> lck(process.mutex);
		process.running_threads++;
		process.state = ThreadState::RUNNING;
		holder.state = ThreadState::RUNNING;
	}

	size_t rval = 0;
	try {
		rval = procedure(regs);
	}
	catch (...) {
		Debug_Log("Thread threw an uncaught exception!");
		rval = -1;
	}

	{
		std::lock_guard<std::recursive_mutex> lck(process.mutex);
		holder.state = ThreadState::ZOMBIE;
		Signal_Handle(holder.handle);
		process.running_threads--;
		{
			std::lock_guard<std::recursive_mutex> ttd_lck(Thread_To_Process_Mutex);
			Thread_To_Process.erase(holder.handle);
		}
		if (main_thread) {
			process.exit_code = static_cast<decltype(process.exit_code)>(rval);
		}
		if (!process.running_threads && process.state != ThreadState::ZOMBIE) {
			process.state = ThreadState::ZOMBIE;
			Signal_Handle(process.handle);
		}
	}
}



void Current_Process_Own_Handle(kiv_os::THandle handle) {
	std::lock_guard<std::recursive_mutex> pt_lck(Process_Table_Mutex);
	auto proc_handle = Current_Process_Handle();
	if (proc_handle == kiv_os::Invalid_Handle) {
		// the kernel cannot thread!
		Debug_Log("The kernel thread attempted to own a handle. But wait, that's illegal!");
		return;
	}

	auto it = Process_Table.find(proc_handle);
	if (it == Process_Table.end()) {
		// no such process?? this should never happen
		Debug_Log("Could not find process with handle " << proc_handle << " - this should be impossible!");
		return;
	}

	auto& process = it->second;
	{
		std::lock_guard<decltype(process.mutex)> p_lck(process.mutex);
		process.owned_handles.insert(handle);
	}
}

void Current_Process_Disown_Handle(kiv_os::THandle handle) {
	std::lock_guard<std::recursive_mutex> pt_lck(Process_Table_Mutex);
	auto proc_handle = Current_Process_Handle();
	if (proc_handle == kiv_os::Invalid_Handle) {
		// the kernel cannot thread!
		Debug_Log("The kernel thread attempted to disown a handle. But wait, that's illegal!");
		return;
	}

	auto it = Process_Table.find(proc_handle);
	if (it == Process_Table.end()) {
		// no such process?? this should never happen
		Debug_Log("Could not find process with handle " << proc_handle << " - this should be impossible!");
		return;
	}

	auto& process = it->second;
	{
		std::lock_guard<decltype(process.mutex)> p_lck(process.mutex);
		process.owned_handles.erase(handle);
	}
}

bool Check_Current_Process_Owns_Handle(kiv_os::THandle handle) {
	std::lock_guard<std::recursive_mutex> pt_lck(Process_Table_Mutex);

	auto proc_handle = Current_Process_Handle();
	if (proc_handle == kiv_os::Invalid_Handle) {
		// the kernel may do anything it pleases
		return true;
	}

	auto it = Process_Table.find(proc_handle);
	if (it == Process_Table.end()) {
		// no such process?? this should never happen
		Debug_Log("Could not find process with handle " << proc_handle << " - this should be impossible!");
		return false;
	}

	Process& process = it->second;
	{
		std::lock_guard<decltype(process.mutex)> lck(process.mutex);
		return process.owned_handles.find(handle) != process.owned_handles.end(); // false-positive warning by Visual Studio
	}
}

fs::path Current_Process_Working_Dir() {
	std::lock_guard<std::recursive_mutex> pt_lck(Process_Table_Mutex);

	auto proc_handle = Current_Process_Handle();
	if (proc_handle == kiv_os::Invalid_Handle) {
		// the kernel has no real working directory so let's just return the default one
		return "C:\\";
	}

	auto it = Process_Table.find(proc_handle);
	if (it == Process_Table.end()) {
		// no such process?? this should never happen
		Debug_Log("Could not find process with handle " << proc_handle << " - this should be impossible!");
		return "C:\\";
	}

	Process& process = it->second;
	{
		std::lock_guard<decltype(process.mutex)> lck(process.mutex);
		return process.working_dir;
	}
}

void Set_Current_Process_Working_Dir(fs::path working_dir) {
	std::lock_guard<std::recursive_mutex> pt_lck(Process_Table_Mutex);

	auto proc_handle = Current_Process_Handle();
	if (proc_handle == kiv_os::Invalid_Handle) {
		// the kernel cannot thread!
		Debug_Log("The kernel thread attempted to change its working directory. But wait, that's illegal!");
		return;
	}

	auto it = Process_Table.find(proc_handle);
	if (it == Process_Table.end()) {
		// no such process?? this should never happen
		Debug_Log("Could not find process with handle " << proc_handle << " - this should be impossible!");
		return;
	}

	Process& process = it->second;
	{
		std::lock_guard<decltype(process.mutex)> lck(process.mutex);
		process.working_dir = working_dir;
	}
}



void Send_Signal(Process& process, const kiv_os::NSignal_Id signal) {
	std::lock_guard<std::recursive_mutex> p_lck(process.mutex);

	auto resolved = process.signal_handlers.find(signal);
	if (resolved == process.signal_handlers.end()) {
		// the process has not registered any handler for this signal - this is valid and we just do nothing here
		return;
	}

	kiv_hal::TRegisters regs = { 0 };
	regs.rcx.r = static_cast<uint64_t>(signal);
	resolved->second(regs);
}

void Terminate_Process_Handles(Process& process) {
	std::lock_guard<decltype(process.mutex)> p_lck(process.mutex);
	std::for_each(process.owned_handles.begin(), process.owned_handles.end(), [](auto& hnd) {
		Terminate_Handle(hnd);
	});
}

void Terminate_Process(Process& process, boolean send_signal = true) {
	std::lock_guard<std::recursive_mutex> p_lck(process.mutex);
	std::lock_guard<std::recursive_mutex> ttp_lck(Thread_To_Process_Mutex);

	if (send_signal) {
		Send_Signal(process, kiv_os::NSignal_Id::Terminate);
	}

	Terminate_Process_Handles(process);

	// signal all threads' handles
	std::for_each(process.threads.begin(), process.threads.end(), [](auto& p) {
		Signal_Handle(p.first);
	});

	// Close owned streams
	std::for_each(process.owned_handles.begin(), process.owned_handles.end(), [](auto& hnd) {
		Close_Handle(hnd);
	});

	if (process.state != ThreadState::ZOMBIE) {
		process.state = ThreadState::ZOMBIE;
		Signal_Handle(process.handle);
	}
}

void Terminate_All_Processes() {
	kiv_os::THandle proc_handle = Current_Process_Handle();
	std::vector<kiv_os::THandle> handles;

	{
		std::lock_guard<std::recursive_mutex> pt_lck(Process_Table_Mutex);
		std::for_each(Process_Table.begin(), Process_Table.end(), [](auto& p) {
			Terminate_Process_Handles(p.second);
		});
	}
}

kiv_os::THandle Add_Process_Thread(Process& process, kiv_os::TThread_Proc procedure, kiv_hal::TRegisters& regs, bool main_thread) {
	std::lock_guard<std::recursive_mutex> p_lck(process.mutex);

	auto thr_handle = Alloc_Handle();
	auto& p = process.threads.emplace(std::piecewise_construct,
		std::forward_as_tuple(thr_handle), std::forward_as_tuple(thr_handle, process, procedure, regs, main_thread));

	std::lock_guard<std::recursive_mutex> ttp_lck(Thread_To_Process_Mutex);
	Thread_To_Process.emplace(thr_handle, process.handle);
	return thr_handle;
}

void Create_Process(kiv_hal::TRegisters& regs) {
	std::lock_guard<std::recursive_mutex> pt_lck(Process_Table_Mutex);

	char* exec_name = reinterpret_cast<char*>(regs.rdx.r);
	kiv_os::TThread_Proc main_procedure = reinterpret_cast<kiv_os::TThread_Proc>(GetProcAddress(User_Programs, exec_name));
	if (!main_procedure) {
		// could not find the user program
		Set_Error(regs, kiv_os::NOS_Error::File_Not_Found);
		Debug_Log("Could not find program " << exec_name << "!");
		return;
	}

	auto proc_handle = Alloc_Handle();
	if (proc_handle == kiv_os::Invalid_Handle) {
		Set_Error(regs, kiv_os::NOS_Error::Out_Of_Memory);
		Debug_Log("Could not allocate another handle");
		return;
	}

	auto result = Process_Table.emplace(proc_handle, proc_handle);
	auto& process = result.first->second;

	fs::path working_dir = "C:\\";
	auto parent_handle = Current_Process_Handle();
	if (parent_handle != kiv_os::Invalid_Handle) {
		auto it = Process_Table.find(parent_handle);
		if (it != Process_Table.end()) {
			auto& parent = it->second;

			std::lock_guard<std::recursive_mutex> parent_lck(parent.mutex);
			working_dir = parent.working_dir;
			parent.children.insert(proc_handle);
		}
	}

	{
		std::lock_guard<std::recursive_mutex> proc_lck(process.mutex);

		strncpy_s(process.command, exec_name, CMD_BUFFER_LENGTH);
		process.working_dir = working_dir;
		process.parent = parent_handle;
		kiv_hal::TRegisters process_regs = { 0 };
		process_regs.rdx = regs.rdx;
		process_regs.rdi = regs.rdi;
		process_regs.rax.x = static_cast<decltype(process_regs.rax.x)>((regs.rbx.e & 0xFFFF0000) >> 16);
		process_regs.rbx.x = static_cast<decltype(process_regs.rbx.x)>(regs.rbx.e & 0x0000FFFF);
		Add_Process_Thread(process, main_procedure, process_regs, true);
	}

	regs.rax.x = static_cast<uint16_t>(proc_handle); // OUT
}

void Create_Thread(kiv_hal::TRegisters& regs) {
	std::lock_guard<std::recursive_mutex> pt_lck(Process_Table_Mutex);

	auto proc_handle = Current_Process_Handle();
	if (proc_handle == kiv_os::Invalid_Handle) {
		// the kernel cannot thread!
		Set_Error(regs, kiv_os::NOS_Error::Permission_Denied);
		Debug_Log("The kernel thread attempted to spawn another thread. But wait, that's illegal!");
		return;
	}

	auto it = Process_Table.find(proc_handle);
	if (it == Process_Table.end()) {
		// no such process?? this should never happen
		Set_Error(regs, kiv_os::NOS_Error::Unknown_Error);
		Debug_Log("Could not find process with handle " << proc_handle << " - this should be impossible!");
		return;
	}

	kiv_os::TThread_Proc thread_procedure = reinterpret_cast<kiv_os::TThread_Proc>(regs.rdx.r);

	auto& process = it->second;
	{
		std::lock_guard<std::recursive_mutex> proc_lck(process.mutex);

		kiv_hal::TRegisters thread_registers = { 0 };
		thread_registers.rdi = regs.rdi;
		auto thr_handle = Add_Process_Thread(process, thread_procedure, thread_registers, false);
		regs.rax.x = static_cast<uint16_t>(thr_handle);
	}
}


void Handle_Clone(kiv_hal::TRegisters& regs) {
	switch (static_cast<kiv_os::NClone>(regs.rcx.r)) {
	case kiv_os::NClone::Create_Process:
		Create_Process(regs);
		break;
	case kiv_os::NClone::Create_Thread:
		Create_Thread(regs);
		break;
	default:
		Set_Error(regs, kiv_os::NOS_Error::Invalid_Argument);
	}
}

void Handle_Wait_For(kiv_hal::TRegisters& regs) {
	kiv_hnd::CvHolder holder;
	kiv_os::THandle holder_hnd = Alloc_Handle();

	auto* handles = reinterpret_cast<kiv_os::THandle*>(regs.rdx.r);
	uint64_t hcount = regs.rcx.r;

	bool do_wait = true;
	for (uint64_t i = 0; i < hcount && do_wait; i++) {
		auto handle = handles[i];
		do_wait &= Add_Handle_Listener(handle, i, holder_hnd, holder);
	}

	if (do_wait) {
		std::unique_lock<std::mutex> lck(holder.mutex);
		holder.cv.wait(lck, [&holder]() { return holder.signalled; });
	}

	regs.rax.r = holder.index;

	for (uint64_t i = 0; i < hcount; i++) {
		auto handle = handles[i];
		Remove_Handle_Listener(handle, holder_hnd);
	}

	Remove_Handle(holder_hnd);
}

void Handle_Read_Exit_Code(kiv_hal::TRegisters& regs) {
	std::lock_guard<std::recursive_mutex> pt_lck(Process_Table_Mutex);
	auto parent_handle = Current_Process_Handle();
	auto target_handle = regs.rdx.x;

	{
		auto it = Process_Table.find(target_handle);
		if (it == Process_Table.end()) {
			Set_Error(regs, kiv_os::NOS_Error::Invalid_Argument);
			Debug_Log("Attempt to read exit code of a non-existent process has been made");
			return;
		}

		auto& target = it->second;
		kiv_os::THandle t_parent;
		{
			std::lock_guard<std::recursive_mutex> t_lck(target.mutex);
			if (target.state != ThreadState::ZOMBIE) {
				// cannot read exit code of an un-exited process
				Set_Error(regs, kiv_os::NOS_Error::Permission_Denied);
				Debug_Log("Attempt to read exit code of an un-exited process has been made");
				return; // this warning is a bug in Visual Studio... that's great!
			}

			regs.rcx.x = target.exit_code;
			t_parent = target.parent;
		}

		if (t_parent == parent_handle) {
			// the parent has read the exit code, we can dispose of the process now
			Process_Table.erase(it);
			Remove_Handle(target_handle);
		}
	}

	{
		auto it = Process_Table.find(parent_handle);
		if (it == Process_Table.end()) {
			return;
		}

		auto& parent = it->second;
		{
			std::lock_guard<decltype(parent.mutex)> p_lck(parent.mutex);
			parent.children.erase(target_handle);
		}
	}
}

void Handle_Exit(kiv_hal::TRegisters& regs) {
	{
		std::lock_guard<std::recursive_mutex> pt_lck(Process_Table_Mutex);
		auto proc_handle = Current_Process_Handle();
		if (proc_handle == kiv_os::Invalid_Handle) {
			// the kernel cannot exit like this!
			Set_Error(regs, kiv_os::NOS_Error::Permission_Denied);
			Debug_Log("Exit called in kernel thread!");
			return;
		}

		auto it = Process_Table.find(proc_handle);
		if (it == Process_Table.end()) {
			// no such process?? this should never happen
			Set_Error(regs, kiv_os::NOS_Error::Unknown_Error);
			Debug_Log("Could not find process with handle " << proc_handle << " - this should be impossible!");
			return;
		}

		auto& process = it->second;
		std::lock_guard<std::recursive_mutex> p_lck(process.mutex);
		process.exit_code = regs.rcx.x;
		Terminate_Process(process, false);
	}

	ExitThread(0);
}

void Handle_Shutdown(kiv_hal::TRegisters& regs) {
	Terminate_IO();
}

void Handle_Register_Signal_Handler(kiv_hal::TRegisters& regs) {
	std::lock_guard<std::recursive_mutex> pt_lck(Process_Table_Mutex);
	auto proc_handle = Current_Process_Handle();
	if (proc_handle == kiv_os::Invalid_Handle) {
		// the kernel cannot exit like this!
		Set_Error(regs, kiv_os::NOS_Error::Permission_Denied);
		Debug_Log("Exit called in kernel thread!");
		return;
	}

	auto it = Process_Table.find(proc_handle);
	if (it == Process_Table.end()) {
		// no such process?? this should never happen
		Set_Error(regs, kiv_os::NOS_Error::Unknown_Error);
		Debug_Log("Could not find process with handle " << proc_handle << " - this should be impossible!");
		return;
	}

	auto& process = it->second;
	std::lock_guard<std::recursive_mutex> p_lck(process.mutex);
	auto signal = static_cast<kiv_os::NSignal_Id>(regs.rcx.r);
	if (regs.rdx.r == 0) {
		// un-registering
		process.signal_handlers.erase(signal);
	} else {
		// registering
		process.signal_handlers[signal] = reinterpret_cast<kiv_os::TThread_Proc>(regs.rdx.r);
	}
}


void Handle_Process(kiv_hal::TRegisters& regs) {

	switch (static_cast<kiv_os::NOS_Process>(regs.rax.l)) {
	case kiv_os::NOS_Process::Clone:
		Handle_Clone(regs);
		break;
	case kiv_os::NOS_Process::Wait_For:
		Handle_Wait_For(regs);
		break;
	case kiv_os::NOS_Process::Read_Exit_Code:
		Handle_Read_Exit_Code(regs);
		break;
	case kiv_os::NOS_Process::Exit:
		Handle_Exit(regs);
		break;
	case kiv_os::NOS_Process::Shutdown:
		Handle_Shutdown(regs);
		break;
	case kiv_os::NOS_Process::Register_Signal_Handler:
		Handle_Register_Signal_Handler(regs);
		break;
	default:
		Set_Error(regs, kiv_os::NOS_Error::Invalid_Argument);
	}

}


std::vector<fs::path> Split_Path(fs::path& input) {
	std::vector<fs::path> paths;
	std::for_each(input.begin(), input.end(), [&paths](auto& path) {
		if (!path.empty()) {
			paths.push_back(path);
		}
	});
	return paths;
}

std::vector<kiv_os::TDir_Entry> Proc_FS_List_Root() {
	std::vector<kiv_os::TDir_Entry> entries;

	const kiv_os::NFile_Attributes entry_attrs = kiv_os::NFile_Attributes::Directory
		| kiv_os::NFile_Attributes::Read_Only;

	{
		std::lock_guard<decltype(Process_Table_Mutex)> pt_lck(Process_Table_Mutex);
		std::for_each(Process_Table.begin(), Process_Table.end(), [entry_attrs, &entries](auto& p) {
			kiv_os::TDir_Entry& entry = entries.emplace_back();
			snprintf(entry.file_name, 8, "%u", p.first);
			entry.file_attributes = static_cast<decltype(entry.file_attributes)>(entry_attrs);
		});
	}

	return entries;
}

std::vector<kiv_os::TDir_Entry> Proc_FS_List_Process() {
	std::vector<kiv_os::TDir_Entry> entries;

	const kiv_os::NFile_Attributes entry_attrs = kiv_os::NFile_Attributes::Read_Only;

	kiv_os::TDir_Entry& name_entry = entries.emplace_back();
	strncpy_s(name_entry.file_name, PROC_FS_ENTRY_NAME, sizeof(name_entry.file_name));
	name_entry.file_attributes = static_cast<uint16_t>(entry_attrs);

	kiv_os::TDir_Entry& thrcount_entry = entries.emplace_back();
	strncpy_s(thrcount_entry.file_name, PROC_FS_ENTRY_THRCOUNT, sizeof(thrcount_entry.file_name));
	thrcount_entry.file_attributes = static_cast<uint16_t>(entry_attrs);

	return entries;
}

kiv_os::NOS_Error Proc_FS::Open(fs::path path, std::unique_ptr<Stream>& stream, const kiv_os::NOpen_File open_type, const kiv_os::NFile_Attributes attributes) {
	if (!(static_cast<uint8_t>(attributes) & static_cast<uint8_t>(kiv_os::NFile_Attributes::Read_Only))) {
		// ProcFS is read-only
		return kiv_os::NOS_Error::Permission_Denied;
	}

	std::vector<fs::path> rp = Split_Path(path.relative_path());

	// root directory
	if (rp.empty()) {
		auto entries = Proc_FS_List_Root();
		stream = std::make_unique<Inmemory_Stream>(entries, false);
		return kiv_os::NOS_Error::Success;
	}

	std::lock_guard pt_lck(Process_Table_Mutex);

	kiv_os::THandle prochnd = std::stoi(rp[0].string());
	auto& it = Process_Table.find(prochnd);
	if (it == Process_Table.end()) {
		return kiv_os::NOS_Error::File_Not_Found;
	}

	// process entry
	if (rp.size() == 1) {
		auto entries = Proc_FS_List_Process();
		stream = std::make_unique<Inmemory_Stream>(entries, false);
		return kiv_os::NOS_Error::Success;
	}

	// process data entry
	if (rp.size() == 2) {
		Process& process = it->second;
		std::lock_guard p_lck(process.mutex);

		std::string filename = rp[1].string();
		std::transform(filename.begin(), filename.end(), filename.begin(), ::tolower);
		if (filename == PROC_FS_ENTRY_NAME_S) {
			stream = std::make_unique<Inmemory_Stream>(process.command, strnlen(process.command, sizeof(process.command)), false);
			return kiv_os::NOS_Error::Success;
		}
		if (filename == PROC_FS_ENTRY_THRCOUNT_S) {
			char cntbuf[16] = { 0 };
			snprintf(cntbuf, sizeof(cntbuf), "%zu", process.running_threads);
			stream = std::make_unique<Inmemory_Stream>(cntbuf, strnlen(cntbuf, sizeof(cntbuf)), false);
			return kiv_os::NOS_Error::Success;
		}
	}

	return kiv_os::NOS_Error::File_Not_Found;
}

kiv_os::NOS_Error Proc_FS::Set_File_Attributes(fs::path path, const kiv_os::NFile_Attributes attributes) {
	// ProcFS is read-only
	return kiv_os::NOS_Error::Permission_Denied;
}

kiv_os::NOS_Error Proc_FS::Get_File_Attributes(fs::path path, kiv_os::NFile_Attributes& attributes) {
	attributes = kiv_os::NFile_Attributes::Read_Only;

	std::vector<fs::path> rp = Split_Path(path.relative_path());

	// root directory
	if (rp.empty()) {
		attributes |= kiv_os::NFile_Attributes::Directory;
		return kiv_os::NOS_Error::Success;
	}

	kiv_os::THandle prochnd = std::stoi(rp[0].string());
	{
		std::lock_guard pt_lck(Process_Table_Mutex);
		if (Process_Table.find(prochnd) == Process_Table.end()) {
			return kiv_os::NOS_Error::File_Not_Found;
		}
	}

	// process entry
	if (rp.size() == 1) {
		attributes |= kiv_os::NFile_Attributes::Directory;
		return kiv_os::NOS_Error::Success;
	}

	// process data entry
	if (rp.size() == 2) {
		std::string filename = rp[1].string();
		std::transform(filename.begin(), filename.end(), filename.begin(), ::tolower);
		if (filename == PROC_FS_ENTRY_NAME_S || filename == PROC_FS_ENTRY_THRCOUNT_S) {
			return kiv_os::NOS_Error::Success;
		}
	}

	return kiv_os::NOS_Error::File_Not_Found;
}

kiv_os::NOS_Error Proc_FS::Remove(fs::path path) {
	// ProcFS is read-only
	return kiv_os::NOS_Error::Permission_Denied;
}
