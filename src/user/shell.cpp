#include <vector>
#include <algorithm>

#include "shell.h"
#include "rtl.h"

const char* ROOTPATH = "C:\\";

const size_t TEXT_OUT_SIZE = 256;
const size_t CMD_BUFFER_SIZE = 256;

struct Command_Def {
	char program[CMD_BUFFER_SIZE] = { 0 };
	char args[CMD_BUFFER_SIZE] = { 0 };
	kiv_os::THandle redir_in = kiv_os::Invalid_Handle;
	kiv_os::THandle redir_out = kiv_os::Invalid_Handle;
};

size_t __stdcall shell(const kiv_hal::TRegisters &regs) {
	kiv_os::THandle std_in;
	kiv_os::THandle std_out;
	kiv_os_rtl::Resolve_Std_Io(regs, std_in, std_out);

	size_t counter;
	
	const char* car_ret = "\r";
	const char* new_line = "\n";
	const char* intro = "Welcome to the KIV/OS shell\n";
	kiv_os_rtl::Write_File(std_out, intro, strlen(intro), counter);

	bool show_prompt = true;
	char prompt[TEXT_OUT_SIZE] = "ERROR";

	bool result = kiv_os_rtl::For_Each_Line(std_in,
		// before each line
		[std_out, &prompt, &show_prompt]() {
			if (show_prompt) {
				size_t counter;
				kiv_os_rtl::Get_Working_Dir(prompt, TEXT_OUT_SIZE, counter);
				strncat_s(prompt, ">", TEXT_OUT_SIZE - counter);
				kiv_os_rtl::Write_File(std_out, prompt, strlen(prompt), counter);
			}
			return true;
		},

		// for each line
		[std_in, std_out, new_line, car_ret, &prompt, &show_prompt](const char* line) { // each
			std::vector<Command_Def> cmd_defs;
			cmd_defs.push_back({});
			char err[TEXT_OUT_SIZE] = { 0 };
			size_t counter;
			const char* bp = line;

			kiv_os::THandle file_redir_in = kiv_os::Invalid_Handle;
			kiv_os::THandle file_redir_out = kiv_os::Invalid_Handle;

			bool at_prepended = false;
			if (*bp == '@') {
				++bp;
				at_prepended = true;
			}

			if (show_prompt && !at_prepended) {
				kiv_os_rtl::Write_File(std_out, car_ret, strlen(car_ret), counter);
				kiv_os_rtl::Write_File(std_out, prompt, strlen(prompt), counter);
				kiv_os_rtl::Write_File(std_out, line, strlen(line), counter);
			}

			kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), counter);

			bool quot = false;

			while (true) {
				Command_Def& def = cmd_defs.back();

				char* cp = def.program;
				char* ap = def.args;

				while (*bp && *bp == ' ') {
					++bp; // skip spaces
				}

				while (*bp && *bp != ' ' && *bp != '|' && *bp != '>' && *bp != '<') {
					*cp = *bp;
					++cp;
					++bp;
				}
				*cp = '\0';

				if (!*def.program) {
					// handle empty commands
					if (cmd_defs.size() > 1) {
						snprintf(err, sizeof(err) - 1, "shell: syntax error\n");
						kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);
					}
					return true;
				}

				if (*bp == ' ') {
					// there are arguments, copy them
					while (*bp && *bp == ' ') {
						++bp; // skip spaces
					}
					while (*bp && ((*bp != '|' && *bp != '>' && *bp != '<') || quot)) {
						*ap = *bp;
						if (*bp == '"') {
							quot = !quot;
						}
						++ap;
						++bp;
					}
					do {
						--ap; // go before all spaces after arguments
					} while (ap > def.args && *ap == ' ');
					++ap;
					*ap = '\0';
				} else {
					// no arguments, make them empty
					*ap = '\0';
				}

				if (quot) {
					snprintf(err, sizeof(err) - 1, "shell: syntax error - unclosed quote\n");
					kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);
				}

				// pipe
				if (*bp == '|') {
					// there was a pipe, parse another command
					cmd_defs.push_back({});
					++bp;
					continue;
				}

				// redirects
				while (*bp == '>' || *bp == '<') {
					bool append = false;
					bool set_out = false;

					if (*bp == '>') {
						if (file_redir_out != kiv_os::Invalid_Handle) {
							snprintf(err, sizeof(err), "shell: multiple output redirects specified\n");
							kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);

							kiv_os_rtl::Close_File(file_redir_out);
							kiv_os_rtl::Close_File(file_redir_in);
							return true;
						}
						set_out = true;
						++bp;
						if (*bp == '>') { // >> operator encountered
							append = true;
							++bp;
						}
					} else if (*bp == '<') {
						if (file_redir_in != kiv_os::Invalid_Handle) {
							snprintf(err, sizeof(err), "shell: multiple input redirects specified\n");
							kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);

							kiv_os_rtl::Close_File(file_redir_out);
							kiv_os_rtl::Close_File(file_redir_in);
							return true;
						}
						set_out = false;
						++bp;
					}

					char filename[CMD_BUFFER_SIZE] = { 0 };
					char* fp = filename;
					char* fp_limit = &filename[CMD_BUFFER_SIZE - 1];

					while (*bp && *bp == ' ') {
						++bp; // skip spaces
					}
					while (*bp && *bp != '>' && *bp != '<' && *bp != ' ' && fp < fp_limit) {
						*fp = *bp;
						++fp;
						++bp;
					}
					while (*bp && *bp == ' ') {
						++bp; // skip spaces
					}
					*fp = '\0'; // null-terminate

					if (set_out) {
						file_redir_out = kiv_os_rtl::Open_File(
							filename,
							static_cast<kiv_os::NOpen_File>(0),
							static_cast<kiv_os::NFile_Attributes>(0));

						if (file_redir_out == kiv_os::Invalid_Handle) {
							snprintf(err, sizeof(err) - 1, "shell: could not open output file `%s` for writing\n", filename);
							kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);

							kiv_os_rtl::Close_File(file_redir_in);
							return true;
						}

						if (append) {
							kiv_os_rtl::Seek_File(file_redir_out, 0, kiv_os::NFile_Seek::End);
						} else {
							kiv_os_rtl::Resize_File(file_redir_out, 0, kiv_os::NFile_Seek::Beginning);
						}
					} else {
						file_redir_in = kiv_os_rtl::Open_File(
							filename,
							kiv_os::NOpen_File::fmOpen_Always,
							kiv_os::NFile_Attributes::Read_Only);

						if (file_redir_in == kiv_os::Invalid_Handle) {
							snprintf(err, sizeof(err) - 1, "shell: could not open input file `%s` for writing\n", filename);
							kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);

							kiv_os_rtl::Close_File(file_redir_out);
							return true;
						}
					}
				}

				break;
			}

			// assign file redirections
			if (file_redir_in != kiv_os::Invalid_Handle) {
				cmd_defs.front().redir_in = file_redir_in;
			}
			if (file_redir_out != kiv_os::Invalid_Handle) {
				cmd_defs.back().redir_out = file_redir_out;
			}

			// prepare pipes
			for (size_t i = 0; i < cmd_defs.size() - 1; ++i) {
				kiv_os::THandle pipe[2];
				bool piperesult = kiv_os_rtl::Create_Pipe(pipe);
				if (!piperesult) {
					snprintf(err, sizeof(err) - 1, "shell: could not create a pipe\n");
					kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);
					return true;
				}

				cmd_defs[i].redir_out = pipe[0];
				cmd_defs[i + 1].redir_in = pipe[1];
			}

			// process all the parsed commands
			std::vector<kiv_os::THandle> proc_handles;
			std::vector<size_t> proc_handle_to_def;
			for (size_t i = 0; i < cmd_defs.size(); i++) {
				size_t def_index = cmd_defs.size() - 1 - i;
				Command_Def& def = cmd_defs[def_index];

				if (strcmp(def.program, "exit") == 0) {
					if (cmd_defs.size() > 1) {
						snprintf(err, sizeof(err) - 1, "shell: cannot `exit` in a pipeline\n");
						kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);
						return true;
					}

					return false;
				}

				if (strcmp(def.program, "cd") == 0) {
					if (cmd_defs.size() > 1) {
						snprintf(err, sizeof(err) - 1, "shell: cannot `cd` in a pipeline\n");
						kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);
						return true;
					}

					if (!kiv_os_rtl::Set_Working_Dir(def.args)) {
						snprintf(err, sizeof(err) - 1, "shell: cannot `cd` to `%s`\n", def.args);
						kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);
					}

					return true;
				}

				if (strcmp(def.program, "echo") == 0) {
					if (strcmp(def.args, "on") == 0) {
						if (cmd_defs.size() > 1) {
							snprintf(err, sizeof(err) - 1, "shell: cannot `echo on` in a pipeline\n");
							kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);
							return true;
						}

						show_prompt = true;
						return true;
					} else if (strcmp(def.args, "off") == 0) {
						if (cmd_defs.size() > 1) {
							snprintf(err, sizeof(err) - 1, "shell: cannot `echo off` in a pipeline\n");
							kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);
							return true;
						}

						show_prompt = false;
						return true;
					}
				}

				if (strlen(def.program) == 2 && def.program[1] == ':') {
					if (cmd_defs.size() > 1) {
						snprintf(err, sizeof(err) - 1, "shell: cannot change disk in a pipeline\n");
						kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);
						return true;
					}

					char path[4] = { 0 };
					strncpy_s(path, ROOTPATH, sizeof(path));

					char nd = def.program[0];
					if (nd < 'a' && nd > 'z' && nd < 'A' && nd > 'Z' && nd < '0' && nd > '9') {
						snprintf(err, sizeof(err) - 1, "shell: unexpected drive character '%c'\n", nd);
						kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);
						return true;
					}

					if (nd >= 'a' && nd <= 'z') {
						nd += 'A' - 'a'; // convert to upper
					}

					path[0] = nd;
					kiv_os_rtl::Set_Working_Dir(path);
					return true;
				}

				const kiv_os::THandle in_hnd = (def.redir_in == kiv_os::Invalid_Handle) ? std_in : def.redir_in;
				const kiv_os::THandle out_hnd = (def.redir_out == kiv_os::Invalid_Handle) ? std_out : def.redir_out;

				kiv_os::THandle proc_handle;
				bool proc_created = kiv_os_rtl::Create_Process(def.program, def.args, in_hnd, out_hnd, proc_handle);
				if (!proc_created) {
					if (kiv_os_rtl::Last_Error == kiv_os::NOS_Error::File_Not_Found) {
						snprintf(err, sizeof(err) - 1, "shell: command `%s` not found\n", def.program);
					} else {
						snprintf(err, sizeof(err) - 1, "shell: unexpected error while creating process\n");
					}
					kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);

					// Close all redirects
					for (Command_Def& closing_def : cmd_defs) {
						if (closing_def.redir_in != kiv_os::Invalid_Handle) {
							bool success = kiv_os_rtl::Close_File(closing_def.redir_in);
							if (!success) {
								snprintf(err, sizeof(err) - 1, "shell: error closing input handle for `%s`\n", closing_def.program);
								kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);
							}
						}
						if (closing_def.redir_out != kiv_os::Invalid_Handle) {
							bool success = kiv_os_rtl::Close_File(closing_def.redir_out);
							if (!success) {
								snprintf(err, sizeof(err) - 1, "shell: error closing output handle for `%s`\n", closing_def.program);
								kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);
							}
						}
					}

					// Wait for successfully started programs to exit and clean them up
					while (!proc_handles.empty()) {
						size_t sig_index;
						bool wait_succ = kiv_os_rtl::Wait_For(proc_handles.data(), proc_handles.size(), sig_index);
						if (!wait_succ) {
							snprintf(err, sizeof(err) - 1, "shell: error while waiting for processes\n");
							kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);
							return true;
						}

						kiv_os::THandle closing_proc_handle = proc_handles[sig_index];
						size_t closing_def_index = proc_handle_to_def[sig_index];

						proc_handles.erase(proc_handles.begin() + sig_index);
						proc_handle_to_def.erase(proc_handle_to_def.begin() + sig_index);

						uint16_t exitcode;
						kiv_os_rtl::Read_Exit_Code(closing_proc_handle, exitcode);
					}

					return true;
				}

				proc_handles.push_back(proc_handle);
				proc_handle_to_def.push_back(def_index);
			}

			// Wait for all of the processes and clean them and their pipes up as they exit
			while (!proc_handles.empty()) {
				size_t sig_index;
				bool wait_succ = kiv_os_rtl::Wait_For(proc_handles.data(), proc_handles.size(), sig_index);
				if (!wait_succ) {
					snprintf(err, sizeof(err) - 1, "shell: error while waiting for processes\n");
					kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);
					return true;
				}
				
				kiv_os::THandle proc_handle = proc_handles[sig_index];
				size_t def_index = proc_handle_to_def[sig_index];

				proc_handles.erase(proc_handles.begin() + sig_index);
				proc_handle_to_def.erase(proc_handle_to_def.begin() + sig_index);

				Command_Def& def = cmd_defs[def_index];
				
				if (def.redir_in != kiv_os::Invalid_Handle) {
					bool success = kiv_os_rtl::Close_File(def.redir_in);
					if (!success) {
						snprintf(err, sizeof(err) - 1, "shell: error closing input handle for `%s`\n", def.program);
						kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);
					}
				}
				if (def.redir_out != kiv_os::Invalid_Handle) {
					bool success = kiv_os_rtl::Close_File(def.redir_out);
					if (!success) {
						snprintf(err, sizeof(err) - 1, "shell: error closing output handle for `%s`\n", def.program);
						kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);
					}
				}

				uint16_t exitcode;
				bool ec_read = kiv_os_rtl::Read_Exit_Code(proc_handle, exitcode);
				if (!ec_read) {
					snprintf(err, sizeof(err) - 1, "shell: error while reading process exit code\n");
					kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);
					continue;
				}

				if (exitcode) {
					snprintf(err, sizeof(err) - 1, "shell: command `%s` exited with code %u\n", def.program, exitcode);
					kiv_os_rtl::Write_File(std_out, err, strlen(err), counter);
				}
			}

			return true;
		});

	kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), counter);

	return 0;
}