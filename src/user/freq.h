#ifndef KIV_OS_FREQ_H
#define KIV_OS_FREQ_H

#include "..\api\api.h"

extern "C" size_t __stdcall freq(const kiv_hal::TRegisters &regs);

#endif /* KIV_OS_FREQ_H */
