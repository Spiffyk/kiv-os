#include <list>
#include <algorithm>
#include "rtl.h"

#include "sort.h"

const char new_line = '\n';

size_t __stdcall sort(const kiv_hal::TRegisters& regs) {
	kiv_os::THandle std_in, std_out;
	kiv_os_rtl::Resolve_Std_Io(regs, std_in, std_out);

	std::list<std::string> lines;
	kiv_os_rtl::For_Each_Line(std_in, []() { return true; }, [&lines](const char* line) {
		std::string line_s = line;
		for (auto it = lines.begin(); it != lines.end(); it++) {
			if (line < *it) {
				lines.insert(it, line_s);
				return true;
			}
		}
		lines.push_back(line_s);
		return true;
	});

	std::for_each(lines.begin(), lines.end(), [std_out](std::string& line) {
		size_t counter;
		kiv_os_rtl::Write_File(std_out, line.c_str(), line.size(), counter);
		kiv_os_rtl::Write_File(std_out, &new_line, 1, counter);
	});


	return 0;
}
