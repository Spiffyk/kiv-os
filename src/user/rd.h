#ifndef KIV_OS_RD_H
#define KIV_OS_RD_H

#include "..\api\api.h"

extern "C" size_t __stdcall rd(const kiv_hal::TRegisters &regs);

#endif /* KIV_OS_RD_H */
