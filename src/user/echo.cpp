#include "echo.h"

#include "rtl.h"

size_t __stdcall echo(const kiv_hal::TRegisters& regs) {
	kiv_os::THandle std_in;
	kiv_os::THandle std_out;
	kiv_os_rtl::Resolve_Std_Io(regs, std_in, std_out);

	const char* command = reinterpret_cast<decltype(command)>(regs.rdi.r);
	size_t written;
	const char* new_line = "\n";
	const char* space = " ";

	std::vector<std::string> args;
	kiv_os_rtl::Split_Args(command, args);

	for (std::string& arg : args) {
		kiv_os_rtl::Write_File(std_out, arg.c_str(), arg.size(), written);
		kiv_os_rtl::Write_File(std_out, space, strlen(space), written);
	}
	
	kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), written);
	
	return 0;
}
