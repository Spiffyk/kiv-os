#include "rtl.h"

thread_local kiv_os::NOS_Error kiv_os_rtl::Last_Error;

kiv_hal::TRegisters Prepare_SysCall_Context(kiv_os::NOS_Service_Major major, uint8_t minor) {
	kiv_hal::TRegisters regs;
	regs.rax.h = static_cast<uint8_t>(major);
	regs.rax.l = minor;
	return regs;
}


void kiv_os_rtl::Resolve_Std_Io(const kiv_hal::TRegisters& regs, kiv_os::THandle& std_in, kiv_os::THandle& std_out) {
	std_in = static_cast<kiv_os::THandle>(regs.rax.x);
	std_out = static_cast<kiv_os::THandle>(regs.rbx.x);
}

bool kiv_os_rtl::Read_File(const kiv_os::THandle file_handle, void* const buffer, const size_t buffer_size, size_t &read) {
	kiv_hal::TRegisters regs =  Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Read_File));
	regs.rdx.x = static_cast<decltype(regs.rdx.x)>(file_handle);
	regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(buffer);
	regs.rcx.r = buffer_size;	
	
	const bool result = kiv_os::Sys_Call(regs);
	read = regs.rax.r;
	return result;
}

bool kiv_os_rtl::For_Each_Line(const kiv_os::THandle file_handle, std::function<bool()> before, std::function<bool(const char*)> each) {
	bool result = true;
	bool cont = true;

	const size_t line_buffer_size = 512;
	char line_buffer[line_buffer_size] = { 0 };
	char* lc = line_buffer;
	char* lc_limit = &line_buffer[line_buffer_size - 1];

    while (cont) {
		cont &= before();
		if (!cont) {
			return result;
		}

        const size_t bsize = 128;
        char buf[bsize];
        size_t read;
		result &= Read_File(file_handle, buf, bsize, read);
		if (!result || !read) {
			return result;
		}

		for (size_t i = 0; i < read; ++i) {
			if (lc >= lc_limit) {
				// reached line limit, nothing more I can do :-(
				Last_Error = kiv_os::NOS_Error::Out_Of_Memory;
				return false;
			}

			char c = buf[i];
			switch (static_cast<kiv_hal::NControl_Codes>(c)) {
			case kiv_hal::NControl_Codes::EOT:
			case kiv_hal::NControl_Codes::NUL:
				cont = false;
				[[fallthrough]];
			case kiv_hal::NControl_Codes::CR:
			case kiv_hal::NControl_Codes::LF:
				*lc = '\0';
				cont &= each(line_buffer);
				lc = line_buffer;
				break;

			default:
				*lc = c;
				++lc;
			}
		}
	}

	return result;
}

bool kiv_os_rtl::Write_File(const kiv_os::THandle file_handle, const void *buffer, const size_t buffer_size, size_t &written) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Write_File));
	regs.rdx.x = static_cast<decltype(regs.rdx.x)>(file_handle);
	regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(buffer);
	regs.rcx.r = buffer_size;

	const bool result = kiv_os::Sys_Call(regs);
	written = regs.rax.r;
	return result;
}

bool kiv_os_rtl::Seek_File(const kiv_os::THandle file_handle, const int64_t offset, const kiv_os::NFile_Seek whence) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Seek));
	regs.rdx.x = static_cast<decltype(regs.rdx.x)>(file_handle);
	regs.rdi.r = offset;
	regs.rcx.l = static_cast<decltype(regs.rcx.l)>(whence);
	regs.rcx.h = static_cast<decltype(regs.rcx.h)>(kiv_os::NFile_Seek::Set_Position);

	return kiv_os::Sys_Call(regs);
}

bool kiv_os_rtl::Resize_File(const kiv_os::THandle file_handle, const int64_t offset, const kiv_os::NFile_Seek whence) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Seek));
	regs.rdx.x = static_cast<decltype(regs.rdx.x)>(file_handle);
	regs.rdi.r = offset;
	regs.rcx.l = static_cast<decltype(regs.rcx.l)>(whence);
	regs.rcx.h = static_cast<decltype(regs.rcx.h)>(kiv_os::NFile_Seek::Set_Size);

	return kiv_os::Sys_Call(regs);
}

int64_t kiv_os_rtl::Tell_File(const kiv_os::THandle file_handle) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Seek));
	regs.rdx.x = static_cast<decltype(regs.rdx.x)>(file_handle);
	regs.rcx.h = static_cast<decltype(regs.rcx.h)>(kiv_os::NFile_Seek::Get_Position);

	if (kiv_os::Sys_Call(regs)) {
		return regs.rax.r;
	} else {
		return 0;
	}
}

bool kiv_os_rtl::Create_Process(const char* program_name, const char* args, const kiv_os::THandle in, const kiv_os::THandle out, kiv_os::THandle& proc_handle) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Clone));
	regs.rcx.r = static_cast<decltype(regs.rcx.r)>(kiv_os::NClone::Create_Process);
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(program_name);
	regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(args);
	regs.rbx.e = (in << 16) | out;

	const bool result = kiv_os::Sys_Call(regs);
	proc_handle = regs.rax.x;
	return result;
}

bool kiv_os_rtl::Create_Thread(kiv_os::TThread_Proc program, void *data, kiv_os::THandle &thread_handle) {
    kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Clone));
    regs.rcx.r = static_cast<decltype(regs.rcx.r)>(kiv_os::NClone::Create_Thread);
    regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(program);
    regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(data);

    const bool result = kiv_os::Sys_Call(regs);
    thread_handle = regs.rax.x;
    return result;
}

bool kiv_os_rtl::Wait_For(const kiv_os::THandle* handles, const size_t count, size_t& index) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Wait_For));
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(handles);
	regs.rcx.r = count;

	const bool result = kiv_os::Sys_Call(regs);
	index = regs.rax.r;
	return result;
}

bool kiv_os_rtl::Read_Exit_Code(const kiv_os::THandle handle, uint16_t& code) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Read_Exit_Code));
	regs.rdx.x = handle;

	const bool result = kiv_os::Sys_Call(regs);
	code = regs.rcx.x;
	return result;
}

kiv_os::THandle kiv_os_rtl::Open_File(const char *path, kiv_os::NOpen_File flags, kiv_os::NFile_Attributes attributes) {
    kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Open_File));
    regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(path);
    regs.rcx.l = static_cast<uint8_t>(flags);
    regs.rdi.i = static_cast<uint8_t>(attributes);

    if (kiv_os::Sys_Call(regs)) {
        return regs.rax.x;
    }
    return kiv_os::Invalid_Handle;
}

bool kiv_os_rtl::Close_File(kiv_os::THandle handle) {
	if (handle == kiv_os::Invalid_Handle) {
		// let's just allow this
		return true;
	}

    kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Close_Handle));
    regs.rdx.x = static_cast<decltype(regs.rdx.x)>(handle);

    return kiv_os::Sys_Call(regs);
}

bool kiv_os_rtl::Delete_File(const char *path) {
    kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System,
                                                       static_cast<uint8_t>(kiv_os::NOS_File_System::Delete_File));
    regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(path);

    return kiv_os::Sys_Call(regs);
}

bool kiv_os_rtl::Set_Working_Dir(char* path) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System,
													   static_cast<uint8_t>(kiv_os::NOS_File_System::Set_Working_Dir));
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(path);

	return kiv_os::Sys_Call(regs);
}

bool kiv_os_rtl::Get_Working_Dir(char* buffer, const size_t buffer_size, size_t& written) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System,
													   static_cast<uint8_t>(kiv_os::NOS_File_System::Get_Working_Dir));
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(buffer);
	regs.rcx.r = buffer_size;

	bool result = kiv_os::Sys_Call(regs);
	written = regs.rax.r;
	return result;
}

void kiv_os_rtl::Exit(const uint16_t code) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Exit));
	regs.rcx.x = code;
	kiv_os::Sys_Call(regs);
}

bool kiv_os_rtl::Create_Pipe(kiv_os::THandle* out_handles) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Create_Pipe));
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(out_handles);
	return kiv_os::Sys_Call(regs);
}

void kiv_os_rtl::Split_Args(const char *command, std::vector <std::string> &args) {
    std::string cmd(command);
    size_t len = cmd.length();
    bool qot = false;
    bool sqot = false;
    int arglen;
    for (int i = 0; i < len; i++) {
        int start = i;
        if (cmd[i] == '\"') {
            qot = true;
        } else if (cmd[i] == '\'') {
            sqot = true;
        }

        if (qot) {
            i++;
            start++;
            while (i < len && cmd[i] != '\"') { i++; }
            if (i < len) {
                qot = false;
            }
            arglen = i - start;
            i++;
        } else if (sqot) {
            i++;
            while (i < len && cmd[i] != '\'') { i++; }
            if (i < len) {
                sqot = false;
            }
            arglen = i - start;
            i++;
        } else {
            while (i < len && cmd[i] != ' ') { i++; }
            arglen = i - start;
        }
        args.push_back(cmd.substr(start, arglen));
    }
}

void kiv_os_rtl::Shutdown() {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process,
													   static_cast<uint8_t>(kiv_os::NOS_Process::Shutdown));
	kiv_os::Sys_Call(regs);
}
