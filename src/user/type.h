#ifndef KIV_OS_TYPE_H
#define KIV_OS_TYPE_H

#include "..\api\api.h"

extern "C" size_t __stdcall type(const kiv_hal::TRegisters &regs);

#endif /* KIV_OS_TYPE_H */
