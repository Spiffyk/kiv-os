#include "rtl.h"

#include "tasklist.h"

struct Tasklist_Entry {
	char name[16] = { 0 };
	char pid[16] = { 0 };
	char thrcount[16] = { 0 };
};

std::vector<Tasklist_Entry> Tasklist_Get_Entries() {
	std::vector<Tasklist_Entry> entries;

	const kiv_os::NOpen_File flags = static_cast<kiv_os::NOpen_File>(0);
	const kiv_os::NFile_Attributes dir_attrs = kiv_os::NFile_Attributes::Read_Only | kiv_os::NFile_Attributes::Directory;
	const kiv_os::NFile_Attributes pri_attrs = kiv_os::NFile_Attributes::Read_Only;

	kiv_os::THandle dir_hnd = kiv_os_rtl::Open_File("P:\\", flags, dir_attrs);
	if (dir_hnd == kiv_os::Invalid_Handle) {
		return entries;
	}

	while (true) {
		kiv_os::TDir_Entry dir_entry;
		size_t read;
		bool success = kiv_os_rtl::Read_File(dir_hnd, &dir_entry, sizeof(dir_entry), read);
		if (!success || read != sizeof(dir_entry)) {
			break;
		}

		Tasklist_Entry& entry = entries.emplace_back();
		strncpy_s(entry.pid, dir_entry.file_name, sizeof(entry.pid));

		char pathbuf[128] = { 0 };
		snprintf(pathbuf, sizeof(pathbuf), "P:\\%s\\name.pri", entry.pid);
		kiv_os::THandle hnd = kiv_os_rtl::Open_File(pathbuf, flags, pri_attrs);
		if (hnd == kiv_os::Invalid_Handle) {
			continue;
		}
		kiv_os_rtl::Read_File(hnd, entry.name, sizeof(entry.name), read);
		kiv_os_rtl::Close_File(hnd);

		snprintf(pathbuf, sizeof(pathbuf), "P:\\%s\\thrcount.pri", entry.pid);
		hnd = kiv_os_rtl::Open_File(pathbuf, flags, pri_attrs);
		if (hnd == kiv_os::Invalid_Handle) {
			continue;
		}
		kiv_os_rtl::Read_File(hnd, entry.thrcount, sizeof(entry.thrcount), read);
		kiv_os_rtl::Close_File(hnd);
	}

	kiv_os_rtl::Close_File(dir_hnd);

	return entries;
}

void Tasklist_Print_Entry(kiv_os::THandle output, Tasklist_Entry& entry) {
	size_t counter;
	char buffer[128] = { 0 };
	snprintf(buffer, sizeof(buffer), "%-15s   %6s   %14s\n", entry.name, entry.pid, entry.thrcount);
	kiv_os_rtl::Write_File(output, buffer, strnlen(buffer, sizeof(buffer)), counter);
}

extern "C" size_t __stdcall tasklist(const kiv_hal::TRegisters & regs) {
	size_t counter;
	kiv_os::THandle std_in, std_out;
	kiv_os_rtl::Resolve_Std_Io(regs, std_in, std_out);

	Tasklist_Entry header = { "Command", "PID", "Thread count" };
	Tasklist_Print_Entry(std_out, header);
	Tasklist_Entry separator = { "===============", "======", "==============" };
	Tasklist_Print_Entry(std_out, separator);

	std::vector<Tasklist_Entry> entries = Tasklist_Get_Entries();
	std::for_each(entries.begin(), entries.end(), [std_out](auto& entry) { Tasklist_Print_Entry(std_out, entry); });

	const char* new_line = "\n";
	kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), counter);

	return 0;
}
