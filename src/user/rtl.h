#pragma once

#include <functional>
#include <vector>
#include <string>
#include <filesystem>
#include "..\api\api.h"
#include "util.h"

namespace kiv_os_rtl {

	extern thread_local kiv_os::NOS_Error Last_Error;

	/**
	* Resolves standard input and output handles from `regs` and writes them into `std_in` and `std_out`.
	*/
	void Resolve_Std_Io(const kiv_hal::TRegisters& regs, kiv_os::THandle& std_in, kiv_os::THandle& std_out);

	/**
	* Reads data from the specified `file_handle`, into the specified `buffer` of `buffer_size`. The actual size of
	* the read data is written onto the `read` address.
	*
	* @return `true` on success, otherwise `false`
	*/
	bool Read_File(const kiv_os::THandle file_handle, void* const buffer, const size_t buffer_size, size_t &read);

	/**
	* Reads text lines from the specified stream and processes them according to the specified functions.
	*
	* @param file_handle - handle identifying the stream to read from
	* @param before - a function performed before initiating each read; returns a boolean value to determine whether
	*				  the reading should continue
	* @param each - a function taking the read line as a parameter and returning a boolean value to determine whether
	*				the reading should continue
	*
	* @return `true` on success, otherwise `false`
	*/
	bool For_Each_Line(const kiv_os::THandle file_handle, std::function<bool()> before, std::function<bool(const char*)> each);

	/**
	* Writes data from the specified `buffer` of `buffer_size` into the specified `file_handle`. The actual size
	* of the written data size is written onto the `written` address.
	*
	* @return `true` on success, otherwise `false`
	*/
	bool Write_File(const kiv_os::THandle file_handle, const void *buffer, const size_t buffer_size, size_t &written);

	/**
	* Moves the cursor in the specified file by `offset` from `whence`. The file must be seekable.
	*/
	bool Seek_File(const kiv_os::THandle file_handle, const int64_t offset, const kiv_os::NFile_Seek whence);

	/**
	* Moves the cursor in the specified file by `offset` from `whence` and resizes the file
	* so that the position is included. The file must be seekable, writable, and resizable.
	*/
	bool Resize_File(const kiv_os::THandle file_handle, const int64_t offset, const kiv_os::NFile_Seek whence);

	/**
	* Gets the position of the specified file's cursor. If the file is not seekable, returns 0.
	*/
	int64_t Tell_File(const kiv_os::THandle file_handle);

    /**
     * Open file
     */
    kiv_os::THandle Open_File(const char *path, kiv_os::NOpen_File flags, kiv_os::NFile_Attributes attributes);

    /**
     * Close file. If the specified handle is `kiv_os::Invalid_Handle`, simply returns true.
     */
    bool Close_File(kiv_os::THandle handle);

    /**
     * Delete file
     */
    bool Delete_File(const char *path);

	/**
	* Sets the current working directory. The path must always point to a directory, otherwise the call will fail.
	* 
	* @param path - the path to the new working directory; may be relative; must point to a directory
	* 
	* @return `true` on success, otherwise `false`
	*/
	bool Set_Working_Dir(char* path);

	/**
	* Reads the current working directory.
	* 
	* @param buffer - the target buffer where the directory path will be written
	* @param buffer_size - the size of the target buffer
	* @param written - the amount of actually written bytes
	* 
	* @return `true` on success, otherwise `false`
	*/
	bool Get_Working_Dir(char* buffer, const size_t buffer_size, size_t& written);

	/**
	* Creates a new process from the program specified by `program_name` and passes it arguments specified by `args`.
	* The process's standard input and output are directed towards `in` and `out` respectively. The created process's
	* handle is written into `proc_handle`.
	*
	* @return `true` on success, otherwise `false`
	*/
	bool Create_Process(const char* program_name, const char* args, const kiv_os::THandle in, const kiv_os::THandle out, kiv_os::THandle &proc_handle);

	/**
	 * Create thread
     *
	 * @return `true` on success, otherwise `false`
	 */
    bool Create_Thread(kiv_os::TThread_Proc program, void* data, kiv_os::THandle& thread_handle);

    /**
	* Suspends the current thread until one of the specified handles is signalized. Handles are specified
	* by the `handles` array of size `count`. Once awoken, `index` contains the index of the signalized handle in
	* the `handles` array.
	*
	* @return `true` on success, otherwise `false`
	*/
	bool Wait_For(const kiv_os::THandle* handles, const size_t count, size_t& index);

	/**
	* Reads the exit code of the process specified by `handle`, and writes it into `code`.
	*
	* @return `true` on success, otherwise `false`
	*/
	bool Read_Exit_Code(const kiv_os::THandle handle, uint16_t& code);

	/**
	* Exits the current process, including all of its running threads.
	*/
	void Exit(const uint16_t code);

	/**
	* Creates a new pipe and writes its output into the `out_handles` array. The array must be at least two elements
	* long. The first element will be the pipe writer handle, the second will be the pipe reader handle.
	* 
	* @param out_handles - the output handle array; must be at least of size 2
	* 
	* @return `true` on success, otherwise `false`
	*/
	bool Create_Pipe(kiv_os::THandle* out_handles);

    /**
     * Split command to args
     */
    void Split_Args(const char *command, std::vector <std::string> &args);

	/**
	* Tells the operating system to shut down.
	*/
	void Shutdown();
}
