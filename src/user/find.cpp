#include <vector>
#include <string>
#include "rtl.h"
#include "find.h"

thread_local bool count, inverse;
thread_local std::string str;

bool Get_Args(const char *command, std::vector <std::string> &files) {
    std::string cmd(command);
    size_t len = cmd.length();
    str.resize(0);
    bool qot = false;
    bool param = false;
    count = false;
    inverse = false;
    int arglen;

    for (int i = 0; i < len; i++) {
        int start = i;
        if (cmd[i] == '\"') {
            qot = true;
        } else if (cmd[i] == '/') {
            param = true;
        } else if (cmd[i] == ' ') {
            continue;
        }

        if (param) {
            if (i + 1 >= len) { return false; }
            switch (cmd[i + 1]) {
                case 'c':
                case 'C':
                    count = true;
                    break;
                case 'v':
                case 'V':
                    inverse = true;
                    break;
                default:
                    return false;
            }
            param = false;
            i++;
            continue;
        } else if (qot) {
            i++;
            start++;
            while (i < len && cmd[i] != '\"') { i++; }
            if (i < len) {
                qot = false;
            }
            arglen = i - start;
            i++;

            if (str.empty()) {
                str = cmd.substr(start, arglen);
            } else {
                files.push_back(cmd.substr(start, arglen));
            }
        } else {
            while (i < len && cmd[i] != ' ' && cmd[i] != '\"') { i++; }
            arglen = i - start;
            files.push_back(cmd.substr(start, arglen));
        }
    }

    return true;
}

bool Process_File(kiv_os::THandle in_file, kiv_os::THandle out_file, std::string *file_name) {
    size_t counter = 0;
    size_t written;

    if (file_name != nullptr) {
        std::string title = "---------- " + *file_name;
        if (!kiv_os_rtl::Write_File(out_file, title.data(), title.size(), written)) {
            return false;
        }
    }

    kiv_os_rtl::For_Each_Line(in_file, []() { return true; }, [&counter, &out_file](const char *line) {
        std::string line_s = line;
        size_t written;

        bool found = (line_s.find(str) != std::string::npos && !str.empty()) || (str.empty() && line_s.empty());
        if(inverse) {
            found = !found;
        }

        if (found) {
            counter++;

            if (!count) {
                line_s = "\n" + line_s;
                if (!kiv_os_rtl::Write_File(out_file, line_s.data(), line_s.size(), written)) {
                    return false;
                }
            }
        }
        return true;
    });

    if (count) {
        std::string count_str = ": " + std::to_string(counter) + "\n";
        if (!kiv_os_rtl::Write_File(out_file, count_str.data(), count_str.size(), written)) {
            return false;
        }
    } else {
        std::string new_line = "\n";
        if (!kiv_os_rtl::Write_File(out_file, new_line.data(), new_line.size(), written)) {
            return false;
        }
    }

    return true;
}

extern "C" size_t __stdcall find(const kiv_hal::TRegisters &regs) {
    const char *command = reinterpret_cast<const char *>(regs.rdi.r);
    std::vector <std::string> files;

    if (!Get_Args(command, files)) {
        return 1;
    }

    kiv_os::THandle std_in, std_out;
    kiv_os_rtl::Resolve_Std_Io(regs, std_in, std_out);

    if (files.empty()) {
        if (!Process_File(std_in, std_out, nullptr)) {
            return 1;
        }
    } else {
        for (auto &file : files) {
            kiv_os::NOpen_File flags = kiv_os::NOpen_File::fmOpen_Always;
            kiv_os::THandle file_handle = kiv_os_rtl::Open_File(file.c_str(), flags,kiv_os::NFile_Attributes::Read_Only);
            if (file_handle == kiv_os::Invalid_Handle) {
                return 1;
            }

            bool result = Process_File(file_handle, std_out, &file);
            kiv_os_rtl::Close_File(file_handle);

            if (!result) {
                return 1;
            }
        }
    }

    return 0;
}
