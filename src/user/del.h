#ifndef KIV_OS_DEL_H
#define KIV_OS_DEL_H

#include "..\api\api.h"

extern "C" size_t __stdcall del(const kiv_hal::TRegisters & regs);

#endif /* KIV_OS_DEL_H */
