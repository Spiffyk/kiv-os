#ifndef KIV_OS_RGEN_H
#define KIV_OS_RGEN_H

#include "..\api\api.h"

extern "C" size_t __stdcall rgen(const kiv_hal::TRegisters &regs);

#endif /* KIV_OS_RGEN_H */
