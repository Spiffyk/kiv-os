#include <vector>
#include <string>
#include "rtl.h"
#include "type.h"

const char CR = 13;
const char LF = 10;

inline bool Is_EOF(char ch) {
    // 0, Ctrl + C, Ctrl + D, Ctrl + Z
    return ch == 0 || ch == 3 || ch == 4 || ch == 26;
}

inline bool Write_New_Line(kiv_os::THandle out_file) {
    size_t written = 0;
    std::string new_line = "\n";
    bool result = kiv_os_rtl::Write_File(out_file, new_line.data(), new_line.size(), written);
    return result && written != 0;
}

bool Type_File(std::string path, kiv_os::THandle out_file) {
    kiv_os::NFile_Attributes attrs = kiv_os::NFile_Attributes::Read_Only;
    kiv_os::NOpen_File flags = kiv_os::NOpen_File::fmOpen_Always;
    kiv_os::THandle handle = kiv_os_rtl::Open_File(path.c_str(), flags, attrs);
    if (handle == kiv_os::Invalid_Handle) {
        return 1;
    }

    const size_t buffer_size = 4096;
    char buffer[buffer_size + 1];
    buffer[buffer_size] = '\0';
    size_t read;
    size_t written;
    char end[2] = {0, 0};

    while (true) {
        bool result = kiv_os_rtl::Read_File(handle, buffer, buffer_size, read);
        if (!result) {
            kiv_os_rtl::Close_File(handle);
            return false;
        } else if (read == 0) {
            break;
        }

        size_t to_write = strnlen(buffer, read);
        result = kiv_os_rtl::Write_File(out_file, buffer, to_write, written);
        if (!result || written == 0) {
            kiv_os_rtl::Close_File(handle);
            return false;
        }

        if (read > 0) {
            end[1] = buffer[read - 1];
        }
        if (read > 1) {
            end[0] = buffer[read - 2];
        }

        if (to_write < read) {
            break;
        }
    }

    if (end[0] != CR && end[1] != LF) {
        Write_New_Line(out_file);
    }

    kiv_os_rtl::Close_File(handle);
    return true;
}

bool Type_Std_In(const kiv_hal::TRegisters &regs) {
    const size_t buffer_size = 4096;
    char buffer[buffer_size];

    kiv_os::THandle std_out = static_cast<kiv_os::THandle>(regs.rbx.e);
    kiv_os::THandle std_in = static_cast<kiv_os::THandle>(regs.rbx.e >> 16);

    size_t read;
    size_t written;
    bool eof = false;

    while (!eof) {
        bool result = kiv_os_rtl::Read_File(std_in, buffer, buffer_size, read);
        if (!result) {
            return false;
        } else if (read == 0) {
            break;
        }

        eof = Is_EOF(buffer[read - 1]);
        if (eof) {
            if (--read == 0) {
                break;
            }
        }

        if (!Write_New_Line(std_out)) { return false; }

        result = kiv_os_rtl::Write_File(std_out, buffer, read, written);
        if (!result || written == 0) {
            return false;
        }

        if (!Write_New_Line(std_out)) { return false; }
    }

    return true;
}

extern "C" size_t __stdcall type(const kiv_hal::TRegisters &regs) {
    const char *command = reinterpret_cast<const char *>(regs.rdi.r);
    std::vector <std::string> args;
    kiv_os_rtl::Split_Args(command, args);

    if (args.empty()) {
        if (!Type_Std_In(regs)) {
            return 1;
        }
    } else {
        kiv_os::THandle out_file = static_cast<kiv_os::THandle>(regs.rbx.e);
        for (auto &arg : args) {
            if (!Type_File(arg, out_file)) {
                return 1;
            }
        }
    }

    return 0;
}