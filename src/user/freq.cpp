#include <vector>
#include <string>
#include "rtl.h"
#include "freq.h"

inline bool Is_EOF(char ch) {
    // 0, Ctrl + C, Ctrl + D, Ctrl + Z
    return ch == 0 || ch == 3 || ch == 4 || ch == 26;
}

inline bool Write_New_Line(kiv_os::THandle out_file) {
    size_t written = 0;
    std::string new_line = "\n";
    bool result = kiv_os_rtl::Write_File(out_file, new_line.data(), new_line.size(), written);
    return result && written != 0;
}

inline void Add_Frequency(char c, std::vector<int> &freqs) {
    unsigned int index = (unsigned char) c;
    freqs[index]++;
}

void Print_Frequencies(std::vector<int> &freqs, kiv_os::THandle out_file) {
    const size_t buffer_size = 30;
    char buffer[buffer_size];
    int size;
    rsize_t written;
    unsigned char c = 0;
    for (auto &item : freqs) {
        if (item > 0) {
            size = sprintf_s(buffer, "0x%hhx : %d\n", c, item);
            kiv_os_rtl::Write_File(out_file, buffer, size, written);
        }
        c++;
    }
}

extern "C" size_t __stdcall freq(const kiv_hal::TRegisters &regs) {
    const char *command = reinterpret_cast<const char *>(regs.rdi.r);
    std::vector <std::string> args;
    kiv_os_rtl::Split_Args(command, args);

    if (args.size() != 0) {
        return 1;
    }

    kiv_os::THandle std_in, std_out;
    kiv_os_rtl::Resolve_Std_Io(regs, std_in, std_out);

    const int freq_buffer_size = 256;
    std::vector<int> freqs;
    freqs.resize(freq_buffer_size, 0);

    const int buffer_size = 1;
    char buffer[buffer_size];
    size_t read;

    while (true) {
        bool result = kiv_os_rtl::Read_File(std_in, buffer, buffer_size, read);
        if (!result) {
            return 1;
        }

        if (Is_EOF(buffer[0]) || read == 0) {
            break;
        }

        Add_Frequency(buffer[0], freqs);
    }

    Write_New_Line(std_out);
    Print_Frequencies(freqs, std_out);

    return 0;
}