#ifndef KIV_OS_FIND_H
#define KIV_OS_FIND_H

#include "..\api\api.h"

extern "C" size_t __stdcall find(const kiv_hal::TRegisters &regs);

#endif /* KIV_OS_FIND_H */
