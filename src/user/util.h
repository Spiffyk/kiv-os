#pragma once

#include "../api/api.h"

inline kiv_os::NFile_Attributes operator|(const kiv_os::NFile_Attributes& a, const kiv_os::NFile_Attributes& b) {
	return static_cast<kiv_os::NFile_Attributes>(static_cast<uint8_t>(a) | static_cast<uint8_t>(b));
}

inline kiv_os::NFile_Attributes& operator|=(kiv_os::NFile_Attributes& a, const kiv_os::NFile_Attributes& b) {
	return a = a | b;
}

inline kiv_os::NFile_Attributes operator&(const kiv_os::NFile_Attributes& a, const kiv_os::NFile_Attributes& b) {
	return static_cast<kiv_os::NFile_Attributes>(static_cast<uint8_t>(a) & static_cast<uint8_t>(b));
}

inline kiv_os::NFile_Attributes& operator&=(kiv_os::NFile_Attributes& a, const kiv_os::NFile_Attributes& b) {
	return a = a & b;
}
