#ifndef KIV_OS_DIR_H
#define KIV_OS_DIR_H

#include "..\api\api.h"

extern "C" size_t __stdcall dir(const kiv_hal::TRegisters & regs);

#endif /* KIV_OS_DIR_H */
