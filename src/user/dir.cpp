#include "dir.h"
#include "rtl.h"
#include <vector>
#include <string>
#include <algorithm>

thread_local bool directories = true;
thread_local bool read_only = true;
thread_local bool hidden = false;
thread_local bool archive = true;
thread_local bool system_file = true;
thread_local bool volume = false;

bool Is_Directory(uint16_t attrs) {
    return attrs & static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory);
}

void Format_File_Name(char *file_name) {
    // make space for dot
    file_name[11] = file_name[10];
    file_name[10] = file_name[9];
    file_name[9] = file_name[8];
    file_name[8] = '.';
}

void Remove_Spaces(std::string &file_name) {
    int pos = 0;
    for (int i = 0; i < 8; i++) {
        if (file_name[pos] == 0x20) {
            file_name.erase(pos, 1);
        } else {
            pos++;
        }
    }
}

bool May_Show_Entry(kiv_os::TDir_Entry entry) {
    if (!directories && entry.file_attributes & static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory)) {
        return false;
    }
    if (!read_only && entry.file_attributes & static_cast<uint8_t>(kiv_os::NFile_Attributes::Read_Only)) {
        return false;
    }
    if (!hidden && entry.file_attributes & static_cast<uint8_t>(kiv_os::NFile_Attributes::Hidden)) {
        return false;
    }
    if (!archive && entry.file_attributes & static_cast<uint8_t>(kiv_os::NFile_Attributes::Archive)) {
        return false;
    }
    if (!system_file && entry.file_attributes & static_cast<uint8_t>(kiv_os::NFile_Attributes::System_File)) {
        return false;
    }
    if (!volume && entry.file_attributes & static_cast<uint8_t>(kiv_os::NFile_Attributes::Volume_ID)) {
        return false;
    }

    return true;
}

bool Print_Dir_Entry(kiv_os::TDir_Entry entry, kiv_os::THandle out_file) {
    for (int i = 0; i < 11; i++) {
        if (entry.file_name[i] == 0) {
            entry.file_name[i] = ' ';
        }
    }

    Format_File_Name(entry.file_name);
    std::string str(entry.file_name);
    str.resize(12);

    if (entry.file_attributes & static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory)) {
        str[8] = ' ';
        str.append("    <DIR>\n");
    } else if(entry.file_attributes & static_cast<uint8_t>(kiv_os::NFile_Attributes::Volume_ID)) {
        str[8] = ' ';
        str.append("    <VOLUME_ID>\n");
    } else {
        Remove_Spaces(str);
        str.append("\n");
    }

    size_t written;
    bool result = kiv_os_rtl::Write_File(static_cast<kiv_os::THandle>(out_file), str.c_str(), str.size(), written);
    if (!result || written == 0) {
        return false;
    }

    return true;
}

bool Process_Dir(std::string path, bool recursively, kiv_os::THandle out_file) {
    kiv_os::NFile_Attributes attrs = kiv_os::NFile_Attributes::Directory | kiv_os::NFile_Attributes::Read_Only;
    kiv_os::NOpen_File flags = kiv_os::NOpen_File::fmOpen_Always;
    kiv_os::THandle handle = kiv_os_rtl::Open_File(path.c_str(), flags, attrs);
    if (handle == kiv_os::Invalid_Handle) {
        return 1;
    }

    std::vector <std::string> stack;
    std::vector <kiv_os::TDir_Entry> buffer;
    buffer.resize(30);
    size_t read = 0;

    while (true) {
        bool result = kiv_os_rtl::Read_File(handle, (char *) buffer.data(), buffer.size() * sizeof(kiv_os::TDir_Entry),read);
        if (!result || read == 0) {
            break;
        }

        for (auto &entry : buffer) {
            if (entry.file_name[0] == '\0') {
                break;
            }

            if(May_Show_Entry(entry) && !Print_Dir_Entry(entry, out_file)) {
                break;
            }

            if (recursively && Is_Directory(entry.file_attributes)) {
                if(std::strncmp(entry.file_name, ".", 1) != 0 && std::strncmp(entry.file_name, "..", 2) != 0) {
                    stack.push_back(path + "/" + entry.file_name);
                }
            }
        }
    }

    kiv_os_rtl::Close_File(handle);

    for (auto &dir : stack) {
        if (!Process_Dir(dir, recursively, out_file)) {
            return false;
        }
    }

    return true;
}

bool Parse_Attr_Options(std::string& options) {
    int i_directories = -1;
    int i_read_only = -1;
    int i_hidden = -1;
    int i_archive = -1;
    int i_system_file = -1;

    bool not = false;
    bool only_not = false;

    for (int i = 0; i < options.size(); i++) {
        switch (options[i]){
        case ':':
            continue;
        case '-':
            not = true;
            continue;
        case 'D':
        case 'd':
            i_directories = not ? 0 : 1;
            break;
        case 'R':
        case 'r':
            i_read_only = not ? 0 : 1;
            break;
        case 'H':
        case 'h':
            i_hidden = not ? 0 : 1;
            break;
        case 'A':
        case 'a':
            i_archive = not ? 0 : 1;
            break;
        case 'S':
        case 's':
            i_system_file = not ? 0 : 1;
            break;
        default:
            return false;
        }

        if (!only_not) {
            only_not = not;
        }
        not = false;
    }

    if (options.empty()) {
        directories = true;
        read_only = true;
        volume = hidden = true;
        archive = true;
        system_file = true;
        return true;
    }

    if (only_not) {
        directories = i_directories == -1 ? true : false;
        read_only = i_read_only == -1 ? true : false;
        volume = hidden = i_hidden == -1 ? true : false;
        archive = i_archive == -1 ? true : false;
        system_file = i_system_file == -1 ? true : false;
    } else {
        directories = i_directories == -1 ? false : true;
        read_only = i_read_only == -1 ? false : true;
        volume = hidden = i_hidden == -1 ? false : true;
        archive = i_archive == -1 ? false : true;
        system_file = i_system_file == -1 ? false : true;
    }
    
    return true;
}

extern "C" size_t __stdcall dir(const kiv_hal::TRegisters &regs) {
    const char *command = reinterpret_cast<const char *>(regs.rdi.r);
    std::vector <std::string> args;
    kiv_os_rtl::Split_Args(command, args);

    std::string attr_option;
    bool use_attr_option = false;
    bool recursively = false;
    for (auto it = args.begin(); it != args.end();) {
        if (it->compare("/s") == 0 || it->compare("/S") == 0) {
            recursively = true;
            it = args.erase(it);
        } else if (it->compare(0, 2, "/a") == 0 || it->compare(0, 2, "/A") == 0) {
            use_attr_option = true;
            attr_option = *it;
            attr_option.erase(0, 2);
            it = args.erase(it);
            continue;
        } else {
            ++it;
        }
    }

    if (use_attr_option && !Parse_Attr_Options(attr_option)) {
        return 1;
    } else if (!use_attr_option) {
        directories = true;
        read_only = true;
        volume = hidden = false;
        archive = true;
        system_file = true;
    }

    if (args.empty()) {
        args.push_back("");
    }

    kiv_os::THandle out_file = static_cast<kiv_os::THandle>(regs.rbx.e);
    for (auto &arg : args) {
        Process_Dir(arg, recursively, out_file);
    }

    return 0;
}