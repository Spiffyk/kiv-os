#include <vector>
#include <string>
#include "rtl.h"
#include "del.h"

bool Delete_File(std::string file) {
    kiv_os::NFile_Attributes attrs = static_cast<kiv_os::NFile_Attributes>(0);
    kiv_os::NOpen_File flags = kiv_os::NOpen_File::fmOpen_Always;
    kiv_os::THandle handle = kiv_os_rtl::Open_File(file.c_str(), flags, attrs);
    if (handle == kiv_os::Invalid_Handle) {
        return 1;
    }
    kiv_os_rtl::Close_File(handle);

    return kiv_os_rtl::Delete_File(file.c_str());
}

extern "C" size_t __stdcall del(const kiv_hal::TRegisters &regs) {
    const char *command = reinterpret_cast<const char *>(regs.rdi.r);
    std::vector <std::string> args;
    kiv_os_rtl::Split_Args(command, args);

    for (auto &arg : args) {
        if (!Delete_File(arg)) {
            return 1;
        }
    }

    return 0;
}