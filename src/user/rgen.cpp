#include <vector>
#include <string>
#include <random>
#include <mutex>
#include "rtl.h"
#include "rgen.h"

class Rgen_Thread_Data_Block {
    std::mutex mutex;

    bool eof = false;
    kiv_os::THandle std_in = kiv_os::Invalid_Handle;

public:

    inline bool Is_Eof() {
        std::lock_guard lck(mutex);
        return eof;
    }

    inline void Set_Eof(bool value) {
        std::lock_guard lck(mutex);
        eof = value;
    }

    inline kiv_os::THandle Get_Std_In() {
        std::lock_guard lck(mutex);
        return std_in;
    }

    inline void Set_Std_In(kiv_os::THandle handle) {
        std::lock_guard lck(mutex);
        std_in = handle;
    }
};

inline bool Is_EOF(char ch) {
    // 0, Ctrl + C, Ctrl + D, Ctrl + Z
    return ch == 0 || ch == 3 || ch == 4 || ch == 26;
}

size_t __stdcall Check_EOF(const kiv_hal::TRegisters &regs) {
    Rgen_Thread_Data_Block& data_block = *reinterpret_cast<Rgen_Thread_Data_Block*>(regs.rdi.r);
    kiv_os::THandle std_in = data_block.Get_Std_In();

    const int buffer_size = 1;
    char buffer[buffer_size];
    while (!data_block.Is_Eof()) {
        size_t read;
        if (!kiv_os_rtl::Read_File(std_in, buffer, buffer_size, read)) {
            data_block.Set_Eof(true);
            return 1;
        }

        if (!read || Is_EOF(buffer[0])) {
            break;
        }
    }

    data_block.Set_Eof(true);
    return 0;
}

void Generate(kiv_os::THandle std_out, Rgen_Thread_Data_Block& data_block) {
    std::default_random_engine generator;
    std::uniform_real_distribution<double> distribution;

    size_t written;

    while (!data_block.Is_Eof()) {
        double number = distribution(generator);
        std::string str = std::to_string(number);
        str.append("\n");

        if (!kiv_os_rtl::Write_File(std_out, str.c_str(), str.size(), written)) {
            data_block.Set_Eof(true);
            break;
        }
    }
}

extern "C" size_t __stdcall rgen(const kiv_hal::TRegisters &regs) {
    const char *command = reinterpret_cast<const char *>(regs.rdi.r);
    std::vector <std::string> args;
    kiv_os_rtl::Split_Args(command, args);
    if (args.size() != 0) {
        return 1;
    }

    kiv_os::THandle std_in, std_out;
    kiv_os_rtl::Resolve_Std_Io(regs, std_in, std_out);

    Rgen_Thread_Data_Block data_block;
    data_block.Set_Std_In(std_in);

    kiv_os::THandle thread_handle;
    bool result = kiv_os_rtl::Create_Thread(Check_EOF, &data_block, thread_handle);
    if (!result) {
        return 1;
    }

    Generate(std_out, data_block);

    std::vector<kiv_os::THandle> single_handle;
    single_handle.push_back(thread_handle);
    size_t index;

    kiv_os_rtl::Wait_For(single_handle.data(), 1, index);

    return 0;
}