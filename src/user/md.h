#ifndef KIV_OS_MD_H
#define KIV_OS_MD_H

#include "..\api\api.h"

extern "C" size_t __stdcall md(const kiv_hal::TRegisters &regs);

#endif /* KIV_OS_MD_H */
