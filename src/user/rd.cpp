#include <vector>
#include <string>
#include "rtl.h"
#include "rd.h"

#define SELF_NAME ".           "
#define PARENT_NAME "..         "

bool Remove_Dir(std::string dir) {
    kiv_os::NFile_Attributes attrs = kiv_os::NFile_Attributes::Directory;
    kiv_os::NOpen_File flags = kiv_os::NOpen_File::fmOpen_Always;
    kiv_os::THandle handle = kiv_os_rtl::Open_File(dir.c_str(), flags, attrs);
    if (handle == kiv_os::Invalid_Handle) {
        return 1;
    }

    std::vector <kiv_os::TDir_Entry> buffer;
    buffer.resize(30);
    size_t read = 0;

    while (true) {
        bool result = kiv_os_rtl::Read_File(handle, (char *) buffer.data(), buffer.size() * sizeof(kiv_os::TDir_Entry),
                                            read);
        if (!result || read == 0) {
            break;
        }

        for (auto &entry : buffer) {
            if (entry.file_name[0] == '\0') {
                break;
            }

            if (strncmp(entry.file_name, SELF_NAME, 11) == 0) {
                continue;
            }

            if (strncmp(entry.file_name, PARENT_NAME, 11) == 0) {
                continue;
            }

            if (entry.file_attributes & static_cast<uint8_t>(kiv_os::NFile_Attributes::Directory)) {
                if (!Remove_Dir(dir + "/" + entry.file_name)) {
                    kiv_os_rtl::Close_File(handle);
                    return false;
                }
            } else if (!kiv_os_rtl::Delete_File(dir.c_str())) {
                kiv_os_rtl::Close_File(handle);
                return false;
            }
        }
    }

    kiv_os_rtl::Close_File(handle);

    if (!kiv_os_rtl::Delete_File(dir.c_str())) {
        return false;
    }

    return true;
}

extern "C" size_t __stdcall rd(const kiv_hal::TRegisters &regs) {
    const char *command = reinterpret_cast<const char *>(regs.rdi.r);
    std::vector <std::string> args;
    kiv_os_rtl::Split_Args(command, args);

    bool recursively = false;
    for (auto it = args.begin(); it != args.end();) {
        if (it->compare("/s") == 0 || it->compare("/S") == 0) {
            recursively = true;
            it = args.erase(it);
            continue;
        }
        ++it;
    }

    if (args.empty()) {
        return 1;
    }

    for(auto &arg : args) {
        if (recursively) {
            if (!Remove_Dir(arg)) {
                return 1;
            }
        } else {
            kiv_os::NFile_Attributes attrs = kiv_os::NFile_Attributes::Directory;
            kiv_os::NOpen_File flags = kiv_os::NOpen_File::fmOpen_Always;
            kiv_os::THandle handle = kiv_os_rtl::Open_File(arg.c_str(), flags, attrs);
            if (handle == kiv_os::Invalid_Handle) {
                return 1;
            }
            kiv_os_rtl::Close_File(handle);

            if (!kiv_os_rtl::Delete_File(arg.c_str())) {
                return 1;
            }
        }
    }

    return 0;
}
