#include "rtl.h"
#include <vector>
#include <string>
#include "md.h"

extern "C" size_t __stdcall md(const kiv_hal::TRegisters &regs) {
    const char *command = reinterpret_cast<const char *>(regs.rdi.r);
    std::vector <std::string> args;
    kiv_os_rtl::Split_Args(command, args);

    if (args.empty()) {
        return 1;
    }

    for(auto &arg : args) {
        kiv_os::NFile_Attributes attrs = kiv_os::NFile_Attributes::Directory;
        kiv_os::NOpen_File flags = static_cast<kiv_os::NOpen_File>(0);
        kiv_os::THandle handle = kiv_os_rtl::Open_File(arg.c_str(), flags, attrs);
        if (handle == kiv_os::Invalid_Handle) {
            return 1;
        }
        kiv_os_rtl::Close_File(handle);
    }

    return 0;
}